<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: rdf_entities_integrator_docnum.class.php,v 1.6.2.1 2017-09-14 14:45:29 tsamson Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

require_once($class_path.'/rdf_entities_integration/rdf_entities_integrator.class.php');
require_once($class_path.'/explnum.class.php');
require_once($include_path.'/explnum.inc.php');
require_once($class_path.'/upload_folder.class.php');

class rdf_entities_integrator_docnum extends rdf_entities_integrator {
	
	protected $table_name = 'explnum';
	
	protected $table_key = 'explnum_id';
	
	protected $ppersos_prefix = 'explnum';
	
	protected function init_map_fields() {
		$this->map_fields = array_merge(parent::init_map_fields(), array(
				'http://www.pmbservices.fr/ontology#bibliographical_lvl' => 'niveau_biblio',
				'http://www.pmbservices.fr/ontology#thumbnail' => 'explnum_vignette',
				'http://www.pmbservices.fr/ontology#label' => 'explnum_nom',
				'http://www.pmbservices.fr/ontology#upload_directory' => 'explnum_repertoire',
				'http://www.pmbservices.fr/ontology#has_docnum_status' => 'explnum_docnum_statut'
		));
		return $this->map_fields;
	}
	
	protected function init_foreign_fields() {
		$this->foreign_fields = array_merge(parent::init_foreign_fields(), array(
				'http://www.pmbservices.fr/ontology#has_record' => 'explnum_notice'
		));
		return $this->foreign_fields;
	}
	
	protected function init_linked_entities() {
		$this->linked_entities = array_merge(parent::init_linked_entities(), array(
				'http://www.pmbservices.fr/ontology#has_concept' => array(
						'table' => 'index_concept',
						'reference_field_name' => 'num_object',
						'external_field_name' => 'num_concept',
						'other_fields' => array(
								'type_object' => TYPE_EXPLNUM
						)
				),
				'http://www.pmbservices.fr/ontology#location' => array(
						'table' => 'explnum_location',
						'reference_field_name' => 'num_explnum',
						'external_field_name' => 'num_location'
				),
				'http://www.pmbservices.fr/ontology#owner' => array(
						'table' => 'explnum_lenders',
						'reference_field_name' => 'explnum_lender_num_explnum',
						'external_field_name' => 'explnum_lender_num_lender'
				)
		));
		return $this->linked_entities;
	}
	
	protected function init_special_fields() {
		$this->special_fields = array_merge(parent::init_special_fields(), array(
				'http://www.pmbservices.fr/ontology#docnum_file' =>  array(
						"method" => array($this,"insert_docnum_file"),
						"arguments" => array('/')
				)
		));
		return $this->special_fields;
	}
	
	public function insert_docnum_file($explnum_path, $values) {
		global $_mimetypes_byext_;

		$ext = pathinfo($values[0]['value'], PATHINFO_EXTENSION);
		if ($ext) {
			// chercher le mimetype associe a l'extension : si trouvee nickel, sinon : ""
			if ($_mimetypes_byext_[$ext]["mimetype"]) $mimetype = $_mimetypes_byext_[$ext]["mimetype"];
		}
		$query = 'UPDATE explnum set explnum_mimetype = "'.$mimetype.'",
				 explnum_extfichier = "'.$ext.'",
				 explnum_nomfichier = "'.$values[0]['value'].'",
				 explnum_path = "'.$explnum_path.'"
				 WHERE explnum_id = '.$this->entity_id;
		pmb_mysql_query($query);
	}
	
	protected function post_create($uri) {
		global $pmb_explnum_controle_doublons, $dbh;
		
		if ($this->entity_id) {
			$query = 'insert into audit (type_obj, object_id, user_id, type_modif, info, type_user) ';
			$query.= 'values ("'.AUDIT_EXPLNUM.'", "'.$this->entity_id.'", "'.$this->contributor_id.'", "'.$this->integration_type.'", "'.addslashes(json_encode(array("uri" => $uri))).'", "'.$this->contributor_type.'")';
			pmb_mysql_query($query);
			
			$explnum = new explnum($this->entity_id);
			$fullname = '';
			if ($explnum->explnum_path) {
				$up = new upload_folder($explnum->explnum_repertoire);
				$fullname = str_replace("//", "/", $explnum->explnum_rep_path . $explnum->explnum_path . $explnum->explnum_nomfichier);
				$fullname = $up->encoder_chaine($fullname);
			}
			$url = isset($explnum->infos_docnum["url"]) ? $explnum->infos_docnum["url"] : "";
			$contenu_vignette = construire_vignette("", $fullname, $url);
			if ($contenu_vignette) {
				$req_mime = "update explnum set explnum_vignette='" . addslashes($contenu_vignette) . "' where explnum_id='" . $this->entity_id . "'";
				pmb_mysql_query($req_mime, $dbh);
			}
			if($fullname) {			
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$mimetype = finfo_file($finfo, $fullname);
				finfo_close($finfo);			
				if(!$mimetype) $mimetype = "application/data";
				$query = 'UPDATE explnum set explnum_mimetype = "'.$mimetype.'"	 WHERE explnum_id = '.$this->entity_id;
				pmb_mysql_query($query);
			}
			
			if ($pmb_explnum_controle_doublons) {
				// On calcule la signature
				pmb_mysql_query("update explnum set explnum_signature='".$explnum->gen_signature()."' where explnum_id=".$this->entity_id);
			}
		}
	}
}