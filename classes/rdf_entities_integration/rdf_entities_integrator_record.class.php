<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: rdf_entities_integrator_record.class.php,v 1.12 2017-06-27 12:50:14 apetithomme Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

require_once($class_path.'/rdf_entities_integration/rdf_entities_integrator.class.php');
require_once($class_path.'/notice.class.php');

class rdf_entities_integrator_record extends rdf_entities_integrator {
	
	protected $table_name = 'notices';
	
	protected $table_key = 'notice_id';
	
	protected $ppersos_prefix = 'notices';
	
	protected function init_map_fields() {
		$this->map_fields = array_merge(parent::init_map_fields(), array(
				'http://www.pmbservices.fr/ontology#bibliographical_lvl' => 'niveau_biblio',
				'http://www.pmbservices.fr/ontology#hierarchical_lvl' => 'niveau_hierar',
				'http://www.pmbservices.fr/ontology#doctype' => 'typdoc',
				'http://www.pmbservices.fr/ontology#tit1' => 'tit1',
				'http://www.pmbservices.fr/ontology#tit2' => 'tit2',
				'http://www.pmbservices.fr/ontology#tit3' => 'tit3',
				'http://www.pmbservices.fr/ontology#tit4' => 'tit4',
				'http://www.pmbservices.fr/ontology#tnvol' => 'tnvol',
				'http://www.pmbservices.fr/ontology#nocoll' => 'nocoll',
				'http://www.pmbservices.fr/ontology#has_date' => 'year',
				'http://www.pmbservices.fr/ontology#publishing_notice' => 'mention_edition',
				'http://www.pmbservices.fr/ontology#isbn' => 'code',
				'http://www.pmbservices.fr/ontology#nb_pages' => 'npages',
				'http://www.pmbservices.fr/ontology#illustration' => 'ill',
				'http://www.pmbservices.fr/ontology#size' => 'size',
				'http://www.pmbservices.fr/ontology#price' => 'prix',
				'http://www.pmbservices.fr/ontology#accompanying_material' => 'accomp',
				'http://www.pmbservices.fr/ontology#general_note' => 'n_gen',
				'http://www.pmbservices.fr/ontology#content_note' => 'n_contenu',
				'http://www.pmbservices.fr/ontology#resume_note' => 'n_resume',
				'http://www.pmbservices.fr/ontology#keywords' => 'index_l',
				'http://www.pmbservices.fr/ontology#url' => 'lien',
				'http://www.pmbservices.fr/ontology#eformat' => 'eformat',
				'http://www.pmbservices.fr/ontology#record_language' => 'indexation_lang',
				'http://www.pmbservices.fr/ontology#new_record' => 'notice_is_new',
				'http://www.pmbservices.fr/ontology#comment' => 'commentaire_gestion',
				'http://www.pmbservices.fr/ontology#thumbnail_url' => 'thumbnail_url',
				'http://www.pmbservices.fr/ontology#thumbnail' => 'thumbnail',
				'http://www.pmbservices.fr/ontology#has_record_status' => 'statut'
		));
		return $this->map_fields;
	}
	
	protected function init_foreign_fields() {
		$this->foreign_fields = array_merge(parent::init_foreign_fields(), array(
				'http://www.pmbservices.fr/ontology#tparent' => 'tparent_id',
				'http://www.pmbservices.fr/ontology#has_publisher' => 'ed1_id',
				'http://www.pmbservices.fr/ontology#has_other_publisher' => 'ed2_id',
				'http://www.pmbservices.fr/ontology#has_collection' => 'coll_id',
				'http://www.pmbservices.fr/ontology#has_subcollection' => 'subcoll_id',
				'http://www.pmbservices.fr/ontology#has_indexint' => 'indexint'
		));
		return $this->foreign_fields;
	}
	
	protected function init_linked_entities() {
		$this->linked_entities = array_merge(parent::init_linked_entities(), array(
				'http://www.pmbservices.fr/ontology#has_concept' => array(
						'table' => 'index_concept',
						'reference_field_name' => 'num_object',
						'external_field_name' => 'num_concept',
						'other_fields' => array(
								'type_object' => TYPE_NOTICE
						)
				),
				'http://www.pmbservices.fr/ontology#has_category' => array(
						'table' => 'notices_categories',
						'reference_field_name' => 'notcateg_notice',
						'external_field_name' => 'num_noeud'
				),
				'http://www.pmbservices.fr/ontology#has_work' => array(
						'table' => 'notices_titres_uniformes',
						'reference_field_name' => 'ntu_num_notice',
						'external_field_name' => 'ntu_num_tu'
				),
				'http://www.pmbservices.fr/ontology#publication_language' => array(
						'table' => 'notices_langues',
						'reference_field_name' => 'num_notice',
						'external_field_name' => 'code_langue',
						'other_fields' => array(
								'type_langue' => '0'
						)
				),
				'http://www.pmbservices.fr/ontology#original_language' => array(
						'table' => 'notices_langues',
						'reference_field_name' => 'num_notice',
						'external_field_name' => 'code_langue',
						'other_fields' => array(
								'type_langue' => '1'
						)
				)
		));
		return $this->linked_entities;
	}
	
	protected function init_special_fields() {
		$this->special_fields = array_merge(parent::init_special_fields(), array(
				'http://www.pmbservices.fr/ontology#has_main_author' => array(
						"method" => array($this,"insert_responsability"),
						"arguments" => array(0)
				),
				'http://www.pmbservices.fr/ontology#has_other_author' => array(
						"method" => array($this,"insert_responsability"),
						"arguments" => array(1)
				),
				'http://www.pmbservices.fr/ontology#has_secondary_author' => array(
						"method" => array($this,"insert_responsability"),
						"arguments" => array(2)
				),
				'http://www.pmbservices.fr/ontology#has_linked_record' => array(
						"method" => array($this,"insert_linked_record"),
						"arguments" => array('up',0)
				),
				'http://www.pmbservices.fr/ontology#has_date' => array(
						"method" => array($this, "insert_parution_date"),
						"arguments" => array()
				)
		));
		return $this->special_fields;
	}
	
	protected function init_base_query_elements() {
		// On d�finit les valeurs par d�faut
		$this->base_query_elements = parent::init_base_query_elements();
		if (!$this->entity_id) {
			$this->base_query_elements = array_merge($this->base_query_elements, array(
					'create_date' => date('Y-m-d H:i:s')
			));
		}
	}
	
	protected function post_create($uri) {
		if ($this->integration_type && $this->entity_id) {
			// Audit
			$query = 'insert into audit (type_obj, object_id, user_id, type_modif, info, type_user) ';
			$query.= 'values ("'.AUDIT_NOTICE.'", "'.$this->entity_id.'", "'.$this->contributor_id.'", "'.$this->integration_type.'", "'.addslashes(json_encode(array("uri" => $uri))).'", "'.$this->contributor_type.'")';
			pmb_mysql_query($query);

			switch($this->integration_type) {
				case "1":
				case "3":
					notice::manage_access_rights($this->entity_id, true);
					break;
				case "2":
					notice::manage_access_rights($this->entity_id);
					break;
			}
		}
		if ($this->entity_id) {
			// Indexation
			notice::majNoticesTotal($this->entity_id);
		}
	}
	
	public function insert_responsability($responsability_type, $values) {
		$query = "	DELETE FROM responsability
					WHERE responsability_notice = '".$this->entity_id."'
					AND responsability_type = '".$responsability_type."'";
		pmb_mysql_query($query);

		$query_values = "";
		foreach($values as $value) {
			$author = $this->integrate_entity($value["value"]);
			$this->entity_data['children'][] = $author;
			if ($query_values) {
				$query_values .= ',';
			}
			// On fixe la fonction � auteur en attendant de trouver une solution pour les responsabilit�s
			$query_values .= "('".$author["id"]."', '".$this->entity_id."', '070', '".$responsability_type."')";
		}
		$query = "	INSERT INTO responsability (responsability_author, responsability_notice, responsability_fonction, responsability_type) 
					VALUES ".$query_values;
		pmb_mysql_query($query);
	}
	
	public function insert_linked_record($direction, $num_reverse_link, $values) {
		$query = "	DELETE FROM notices_relations
					WHERE num_notice = '".$this->entity_id."'
					AND direction = '".$direction."'
					AND num_reverse_link = '".$num_reverse_link."'";
		pmb_mysql_query($query);
		
		$query_values = "";
		$rank = 1;
		foreach($values as $value) {
			$record = $this->store->get_property($value["value"],"pmb:has_record");
			$relation_type = $this->store->get_property($value["value"],"pmb:relation_type");
			$record = $this->integrate_entity($record[0]["value"]);
			$this->entity_data['children'][] = $record;
			if ($query_values) {
				$query_values .= ',';
			}
			$query_values .= "('".$this->entity_id."', '".$record["id"]."', '".$relation_type[0]["value"]."', '".$rank."', '".$direction."', '".$num_reverse_link."')";
			$rank++;
		}
		$query = "	INSERT INTO notices_relations (num_notice, linked_notice, relation_type, rank, direction, num_reverse_link)
					VALUES ".$query_values;
		pmb_mysql_query($query);
	}
	
	public function insert_parution_date($values) {
		$date_parution_notice = notice::get_date_parution($values[0]['value']);
		$query = 'update '.$this->table_name.' set date_parution = "'.$date_parution_notice.'" where '.$this->table_key.' = "'.$this->entity_id.'"';
		pmb_mysql_query($query);
	}
}