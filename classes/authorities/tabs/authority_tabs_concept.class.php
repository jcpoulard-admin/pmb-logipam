<?php
// +-------------------------------------------------+
// | 2002-2007 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: authority_tabs_concept.class.php,v 1.4 2016-03-08 14:23:07 vtouchard Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

require_once($class_path.'/authorities/tabs/authority_tabs.class.php');

class authority_tabs_concept extends authority_tabs {

	/**
	 * M�thode permettant de r�cup�rer les autorit�s index�es avec ce concept
	 * @param elements_list_tab $tab
	 * @param authority_tabs $authority_tabs
	 */
	protected static function get_tab_concept_authorities($tab, $authority){
		self::get_tab_authorities_indexed_with_concepts($tab, $authority, array($authority->get_num_object()));
	}
}