<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: searcher_records_concepts.class.php,v 1.4 2016-08-17 10:18:30 vtouchard Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

class searcher_records_concepts extends searcher_records {
	
	public function __construct($user_query){
		parent::__construct($user_query);
		$this->field_restrict[]= array(
				'field' => "code_champ",
				'values' => array(36),
				'op' => "and",
				'not' => false
		);
	}
	
	protected function _get_search_type(){
		return parent::_get_search_type()."_concept";
	}
	
	public static function get_full_query_from_authority($id) {
		$query = parent::get_full_query_from_authority($id)." notice_id in (select distinct num_object as notice_id from index_concept where type_object = 1 and num_concept =".$id.")";
		return $query;
	}
}