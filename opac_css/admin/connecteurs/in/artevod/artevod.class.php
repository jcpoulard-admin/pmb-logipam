<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: artevod.class.php,v 1.9 2017-07-12 15:15:01 tsamson Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

global $class_path,$base_path, $include_path;
require_once($class_path."/connecteurs.class.php");
require_once("$class_path/curl.class.php");
require_once($class_path."/parametres_perso.class.php");
require_once($include_path."/parser.inc.php");
require_once($base_path."/cms/modules/common/includes/pmb_h2o.inc.php");
if (version_compare(PHP_VERSION,'5','>=') && extension_loaded('xsl')) {
	if (substr(phpversion(), 0, 1) == "5") @ini_set("zend.ze1_compatibility_mode", "0");
	require_once($include_path.'/xslt-php4-to-php5.inc.php');
}

class artevod extends connector {
	//Variables internes pour la progression de la r�cup�ration des notices
	public $profile;				//Profil ArteVOD
	public $n_recu;				//Nombre de notices re�ues
	public $xslt_transform;		//Feuille xslt transmise
	
	public function __construct($connector_path="") {
		parent::__construct($connector_path);
		$xml=file_get_contents($connector_path."/profil.xml");
		$this->profile=_parser_text_no_function_($xml,"ARTEVODCONFIG");
	}
    
    public function get_id() {
    	return "artevod";
    }
    
    public function get_token(){
    	global $empr_cb, $empr_nom, $empr_prenom, $empr_mail, $empr_year;
    	$infos = unserialize($this->parameters);
    	if($_SESSION['user_code'] && isset($infos['privatekey'])) {
			$id_encrypted = hash('sha256', $empr_cb.$infos['privatekey']);
			return "http://portal.mediatheque-numerique.com/sso_login?sso_id=mednum&id=".$empr_cb."&email=".$empr_mail."&nom=".strtolower($empr_nom)."&prenom=".strtolower($empr_prenom)."&dnaiss=".$empr_year."-12-31&id_encrypted=".$id_encrypted."&return_url=";
		}
		return '';
    }
    
    //Est-ce un entrepot ?
	public function is_repository() {
		return 1;
	}
    
   	public function source_get_property_form($source_id) {
    	$params=$this->get_source_params($source_id);
		if ($params["PARAMETERS"]) {
			//Affichage du formulaire avec $params["PARAMETERS"]
			$vars=unserialize($params["PARAMETERS"]);
			foreach ($vars as $key=>$val) {
				global ${$key};
				${$key}=$val;
			}	
		}
		$searchindexes=$this->profile["SEARCHINDEXES"][0]["SEARCHINDEX"];
		if (!$url) $url=$searchindexes[0]["URL"];
		$form = "";
		if (count($searchindexes) > 1) {
			$form .= "
			<div class='row'>
				<div class='colonne3'>
					<label for='search_indexes'>".$this->msg["artevod_search_in"]."</label>
				</div>
				<div class='colonne_suite'>
					<select name='url' id='url' >";
				for ($i=0; $i<count($searchindexes); $i++) {
					$form.="<option value='".$searchindexes[$i]["URL"]."' ".($url==$searchindexes[$i]["URL"]?"selected":"").">".$this->get_libelle($searchindexes[$i]["COMMENT"])."</option>\n";
				}
				$form.="
				</select>
				</div>
			</div>";
		} else {
			$form .= "
			<input type='hidden' id='url' name='url' value='".$searchindexes[0]["URL"]."' />
			";
		}
		$form .= "<div class='row'>
			<div class='colonne3'>
				<label for='xslt_file'>".$this->msg["artevod_xslt_file"]."</label>
			</div>
			<div class='colonne_suite'>
				<input name='xslt_file' type='file'/>";
		if ($xsl_transform) $form.="<br /><i>".sprintf($this->msg["artevod_xslt_file_linked"],$xsl_transform["name"])."</i> : ".$this->msg["artevod_del_xslt_file"]." <input type='checkbox' name='del_xsl_transform' value='1'/>";
		$form.="</div>
		</div>";
		$form .= "<div class='row'></div>";
		return $form;
    }
    
    public function make_serialized_source_properties($source_id) {
    	global $url;
    	global $del_xsl_transform;
    	 
    	$t["url"]=$url;
    	 
    	//V�rification du fichier
    	if (($_FILES["xslt_file"])&&(!$_FILES["xslt_file"]["error"])) {
    		$xslt_file_content=array();
    		$xslt_file_content["name"]=$_FILES["xslt_file"]["name"];
    		$xslt_file_content["code"]=file_get_contents($_FILES["xslt_file"]["tmp_name"]);
    		$t["xsl_transform"]=$xslt_file_content;
    	} else if ($del_xsl_transform) {
    		$t["xsl_transform"]="";
    	} else {
    		$oldparams=$this->get_source_params($source_id);
    		if ($oldparams["PARAMETERS"]) {
    			//Anciens param�tres
    			$oldvars=unserialize($oldparams["PARAMETERS"]);
    		}
    		$t["xsl_transform"] = $oldvars["xsl_transform"];
    	}
    	
    	$this->sources[$source_id]["PARAMETERS"]=serialize($t);
    }
    
    //Formulaire des propri�t�s g�n�rales
    public function get_property_form() {
    	global $charset;
    	$this->fetch_global_properties();
    	//Affichage du formulaire en fonction de $this->parameters
    	if ($this->parameters) {
    		$keys = unserialize($this->parameters);
    		$accesskey= $keys['accesskey'];
    		$secretkey=$keys['secretkey'];
    		$privatekey=$keys['privatekey'];
    	} else {
    		$accesskey="";
    		$secretkey="";
    		$privatekey="";
    	}
    	$r="<div class='row'>
				<div class='colonne3'><label for='accesskey'>".$this->msg["artevod_key"]."</label></div>
				<div class='colonne-suite'><input type='text' id='accesskey' name='accesskey' value='".htmlentities($accesskey,ENT_QUOTES,$charset)."'/></div>
			</div>
			<div class='row'>
				<div class='colonne3'><label for='secretkey'>".$this->msg["artevod_secret_key"]."</label></div>
				<div class='colonne-suite'><input type='text' class='saisie-50em' id='secretkey' name='secretkey' value='".htmlentities($secretkey,ENT_QUOTES,$charset)."'/></div>
			</div>
			<div class='row'>
				<div class='colonne3'><label for='privatekey'>".$this->msg["artevod_private_key"]."</label></div>
				<div class='colonne-suite'><input type='text' class='saisie-50em' id='privatekey' name='secretkey' value='".htmlentities($privatekey,ENT_QUOTES,$charset)."'/></div>
			</div>";
    	return $r;
    }
    
    public function make_serialized_properties() {
    	global $accesskey, $secretkey, $privatekey;
    	//Mise en forme des param�tres � partir de variables globales (mettre le r�sultat dans $this->parameters)
    	$keys = array();
    	 
    	$keys['accesskey']=$accesskey;
    	$keys['secretkey']=$secretkey;
    	$keys['privatekey']=$privatekey;
    	$this->parameters = serialize($keys);
    }
    
    public function maj_entrepot($source_id,$callback_progress="",$recover=false,$recover_env="") {
    	global $charset,$base_path;
    	
    	$this->fetch_global_properties();
    	$keys = unserialize($this->parameters);

		$this->callback_progress=$callback_progress;
		$params=$this->unserialize_source_params($source_id);
		$p=$params["PARAMETERS"];
		$this->source_id=$source_id;
		$this->n_recu=0;
		
		//R�cup�ration du fichier XML distant en cURL
		$xml="";
		if(strpos($p["url"],"?")) {
			$url = substr($p["url"],0,strpos($p["url"],"?"));
		} else {
			$url = $p["url"];
		}
			
		$aCurl = new Curl();
		$aCurl->timeout=60;
		@mysql_set_wait_timeout();
		
		//Authentification Basic
		if (substr($url,0,7) == "http://") {
			$auth_basic = "http://".$keys["accesskey"].":".$keys["secretkey"]."@".substr($url,7);
		} elseif (substr($url,0,8) == "https://") {
			$auth_basic = "https://".$keys["accesskey"].":".$keys["secretkey"]."@".substr($url,8);
		} else {
			$auth_basic = $keys["accesskey"].":".$keys["secretkey"]."@".$url;
		}
			
		//On fait un premier appel pour r�cup�rer le nombre total de documents
 		$url_temp_auth_basic = $auth_basic."?partial=0&page_size=0";
		$content = $aCurl->get($url_temp_auth_basic);
 		$xml_content=$content->body;
 			
 		if($xml_content && $content->headers['Status-Code'] == 200){
 			$xsl_transform=$p["xsl_transform"]["code"];
 			if($xsl_transform){
 				if($xsl_transform['code'])
 					$xsl_transform_content = $xsl_transform['code'];
 				else $xsl_transform_content = "";
 			}
 			if($xsl_transform_content == "") {
 				$xsl_transform_content = file_get_contents($base_path."/admin/connecteurs/in/artevod/xslt/artevod_to_pmbxmlunimarc.xsl");
 			}
 			$params = _parser_text_no_function_($xml_content,"WSOBJECTLISTQUERY");
 			if($params["TOTAL_COUNT"]) {
 				$this->n_total = $params["TOTAL_COUNT"]; 
 				$nb = 0;
 				$nb_per_pass = 50;
 				$page_nb = 1;
 				while ($nb <= $params["TOTAL_COUNT"]) {
 				 	$url_temp_auth_basic = $auth_basic."?partial=0&page_size=".$nb_per_pass."&page_nb=".$page_nb;
 				 	$content = $aCurl->get($url_temp_auth_basic);
 				 	$xml_content=$content->body;
 				 	if($xml_content && $content->headers['Status-Code'] == 200){
 				 		$pmbxmlunimarc = $this->apply_xsl_to_xml($xml_content, $xsl_transform_content);
 				 		$this->rec_records($pmbxmlunimarc, $this->source_id,'');
 				 	}
 				 	$page_nb++;
 				 	$nb = $nb + $nb_per_pass;
 				}
 			}
 		} else {
 			$this->error=true;
 			$this->error_message=$this->msg["artevod_error_auth"];
 		}
		
		return $this->n_recu;
    }
    
    public function progress() {
    	$callback_progress=$this->callback_progress;
		if ($this->n_total) {
			$percent =($this->n_recu / $this->n_total);
			$nlu = $this->n_recu;
			$ntotal = $this->n_total;
		} else {
			$percent=0;
			$nlu = $this->n_recu;
			$ntotal = "inconnu";
		}
		call_user_func($callback_progress,$percent,$nlu,$ntotal);
    }
    
    public function rec_records($noticesxml, $source_id, $search_id) {
    	global $charset,$base_path;
    	if (!trim($noticesxml))
    		return;
    
    	$rec_uni_dom=new xml_dom($noticesxml,$charset);
    	$notices=$rec_uni_dom->get_nodes("unimarc/notice");
    	foreach ($notices as $anotice) {
    		$this->rec_record($rec_uni_dom, $anotice, $source_id, $search_id);
    	}
    }
    
	public function rec_record($rec_uni_dom, $noticenode, $source_id, $search_id) {
    	global $charset,$base_path,$dbh;
    	
    	if (!$rec_uni_dom->error) {
    		//Initialisation
    		$ref="";
    		$ufield="";
    		$usubfield="";
    		$field_order=0;
    		$subfield_order=0;
    		$value="";
    		$date_import=date("Y-m-d H:i:s",time());
    			
    		$fs=$rec_uni_dom->get_nodes("f", $noticenode);
    
    		$fs[] = array("NAME" => "f", "ATTRIBS" => array("c" => "1000"), 'TYPE' => 1, "CHILDS" => array(array("DATA" => $search_term, "TYPE" => 2)));
    		
    		//Pas de 001
    		$ref = md5(serialize($noticenode));
    		//Mise � jour
    		if ($ref) {
    			//Si conservation des anciennes notices, on regarde si elle existe
    			if (!$this->del_old) {
    				$ref_exists = $this->has_ref($source_id, $ref);
    			}
    			//Si pas de conservation des anciennes notices, on supprime
    			if ($this->del_old) {
    				$this->delete_from_entrepot($source_id, $ref);
					$this->delete_from_external_count($source_id, $ref);
    			}
    			$ref_exists = false;
    			//Si pas de conservation ou ref�rence inexistante
    			if (($this->del_old)||((!$this->del_old)&&(!$ref_exists))) {
    				//Insertion de l'ent�te
    				$n_header["rs"]=$rec_uni_dom->get_value("unimarc/notice/rs");
    				$n_header["ru"]=$rec_uni_dom->get_value("unimarc/notice/ru");
    				$n_header["el"]=$rec_uni_dom->get_value("unimarc/notice/el");
    				$n_header["bl"]=$rec_uni_dom->get_value("unimarc/notice/bl");
    				$n_header["hl"]=$rec_uni_dom->get_value("unimarc/notice/hl");
    				$n_header["dt"]=$rec_uni_dom->get_value("unimarc/notice/dt");
    					
    				//R�cup�ration d'un ID
    				$recid = $this->insert_into_external_count($source_id, $ref);
    					
    				foreach($n_header as $hc=>$code) {
    					$this->insert_header_into_entrepot($source_id, $ref, $date_import, $hc, $code, $recid, $search_id);
    				}
    				if ($fs)
    				for ($i=0; $i<count($fs); $i++) {
    					$ufield=$fs[$i]["ATTRIBS"]["c"];
    					$field_order=$i;
    					$ss=$rec_uni_dom->get_nodes("s",$fs[$i]);
    					if (is_array($ss)) {
    						for ($j=0; $j<count($ss); $j++) {
    							$usubfield=$ss[$j]["ATTRIBS"]["c"];
    							$value=$rec_uni_dom->get_datas($ss[$j]);
    							$subfield_order=$j;
    							$this->insert_content_into_entrepot($source_id, $ref, $date_import, $ufield, $usubfield, $field_order, $subfield_order, $value, $recid, $search_id);
    						}
    					} else {
    						$value=$rec_uni_dom->get_datas($fs[$i]);
    						$this->insert_content_into_entrepot($source_id, $ref, $date_import, $ufield, $usubfield, $field_order, $subfield_order, $value, $recid, $search_id);
    					}
    				}
    				$this->rec_isbd_record($source_id, $ref, $recid);
    			}
    			$this->n_recu++;
    			$this->progress();
    		}
    	}
    }
    
    public function enrichment_is_allow(){
    	return true;
    }
	
	public function getTypeOfEnrichment($notice_id, $source_id){
		global $dbh;
		
		$params=$this->get_source_params($source_id);
		if ($params["PARAMETERS"]) {
			//Affichage du formulaire avec $params["PARAMETERS"]
			$vars=unserialize($params["PARAMETERS"]);
		}
		
		$type = array();
		
		// On n'affiche l'onglet que si le champ perso est renseign�
		$query = "select 1 from notices_custom_values where notices_custom_champ = ".$vars['cp_field']." and notices_custom_origine= ".$notice_id;
		$result = pmb_mysql_query($query, $dbh);
		if(pmb_mysql_num_rows($result)){
			$type['type'] = array(
				array(
					"code" => "artevod",
					"label" => $this->msg['artevod_vod']
				)
			);		
			$type['source_id'] = $source_id;
		}
		return $type;
	}
	
	public function getEnrichment($notice_id,$source_id,$type="",$enrich_params=array()){
		global $charset;
		
		$params=$this->get_source_params($source_id);
		if ($params["PARAMETERS"]) {
			//Affichage du formulaire avec $params["PARAMETERS"]
			$vars=unserialize($params["PARAMETERS"]);
		}
		$enrichment= array();
		switch ($type){
			case "artevod" :
			default :
				$perso_params = new parametres_perso("notices");
				$perso_params->get_values($notice_id);
				$values = $perso_params->values;
				
				$link = "http://www.mediatheque-numerique.com/ws/films/".$values[$vars['cp_field']][0];
				
				$infos = unserialize($this->parameters);
				
				$curl = new Curl();
				if (isset($infos['accesskey']) && $infos['accesskey']) $curl->set_option("CURLOPT_USERPWD", $infos['accesskey'].":".$infos['secretkey']);
				
				$result = $curl->get($link);
				
				$result = _parser_text_no_function_($result->body, "WSOBJECTQUERY");
				
				$content = "";
				$film = array();
				
				// Titre
				$film['title'] = addslashes($result['FILM'][0]['EDITORIAL'][0]['TITLE'][0]['value']);
				$film['original_title'] = $result['FILM'][0]['EDITORIAL'][0]['ORIGINAL_TITLE'][0]['value'];
				
				// Genres
				$film['genres'] = array();
				if (is_array($result['FILM'][0]['EDITORIAL'][0]['GENRE']) && count($result['FILM'][0]['EDITORIAL'][0]['GENRE'])) {
					foreach ($result['FILM'][0]['EDITORIAL'][0]['GENRE'] as $genre) {
						foreach ($genre['LABEL'] as $label) {
							if ($label['LANG'] == 'fr') {
								$film['genres'][] = $label['value'];
							}
						}
					}
				}
				
				// Sous-genres
				$film['subgenres'] = array();
				if (is_array($result['FILM'][0]['EDITORIAL'][0]['SUB_GENRE']) && count($result['FILM'][0]['EDITORIAL'][0]['SUB_GENRE'])) {
					foreach ($result['FILM'][0]['EDITORIAL'][0]['SUB_GENRE'] as $genre) {
						foreach ($genre['LABEL'] as $label) {
							if ($label['LANG'] == 'fr') {
								$film['subgenres'][] = $label['value'];
							}
						}
					}
				}
				
				// Auteurs
				$film['authors'] = array();
				if (is_array($result['FILM'][0]['STAFF'][0]['AUTHORS'][0]['PERSON']) && count($result['FILM'][0]['STAFF'][0]['AUTHORS'][0]['PERSON'])) {
					foreach ($result['FILM'][0]['STAFF'][0]['AUTHORS'][0]['PERSON'] as $author) {
						if ($author['FULL_NAME'][0]['value']) {
							$film['authors'][] = $author['FULL_NAME'][0]['value'];
						} else {
							$film['authors'][] = $author['FIRST_NAME'][0]['value']." ".$author['LAST_NAME'][0]['value'];
						}
					}
				}
				
				// Acteurs
				$film['actors'] = array();
				if (is_array($result['FILM'][0]['STAFF'][0]['ACTORS'][0]['PERSON']) && count($result['FILM'][0]['STAFF'][0]['ACTORS'][0]['PERSON'])) {
					foreach ($result['FILM'][0]['STAFF'][0]['ACTORS'][0]['PERSON'] as $actor) {
						if ($actor['FULL_NAME'][0]['value']) {
							$film['actors'][] = $actor['FULL_NAME'][0]['value'];
						} else {
							$film['actors'][] = $actor['FIRST_NAME'][0]['value']." ".$actor['LAST_NAME'][0]['value'];
						}
					}
				}
				
				// Couverture
				$film['poster'] = $result['FILM'][0]['MEDIA'][0]['POSTERS'][0]['MEDIA'][0]['SRC'];
				
				// Dur�e
				$film['duration'] = array(
						'raw_value' => $result['FILM'][0]['TECHNICAL'][0]['DURATION'][0]['value'],
						'format_value' => floor($result['FILM'][0]['TECHNICAL'][0]['DURATION'][0]['value']/60).":".str_pad($result['FILM'][0]['TECHNICAL'][0]['DURATION'][0]['value']%60, 2, '0', STR_PAD_LEFT)
				);
				
				// Description
				$film['description'] = addslashes($result['FILM'][0]['EDITORIAL'][0]['DESCRIPTION'][0]['value']);
				
				// R�sum�
				$film['body'] = addslashes($result['FILM'][0]['EDITORIAL'][0]['BODY'][0]['value']);
				
				// Extraits
				$film['trailers'] = array();
				if (is_array($result['FILM'][0]['MEDIA'][0]['TRAILERS'][0]['MEDIA']) && count($result['FILM'][0]['MEDIA'][0]['TRAILERS'][0]['MEDIA'])) {
					foreach ($result['FILM'][0]['MEDIA'][0]['TRAILERS'][0]['MEDIA'] as $trailer) {
						$film['trailers'][] = $trailer['SRC'];
					}
				}
				
				// Photos
				$film['photos'] = array();
				if (is_array($result['FILM'][0]['MEDIA'][0]['PHOTOS'][0]['MEDIA']) && count($result['FILM'][0]['MEDIA'][0]['PHOTOS'][0]['MEDIA'])) {
					foreach ($result['FILM'][0]['MEDIA'][0]['PHOTOS'][0]['MEDIA'] as $photo) {
						$film['photos'][] = $photo['SRC'];
					}
				}
				
				// Public
				$film['target_audience'] = $result['FILM'][0]['TECHNICAL'][0]['TARGET_AUDIENCE'][0]['LABEL'][0]['value'];
				
				// Ann�e de production
				$film['production_year'] = $result['FILM'][0]['TECHNICAL'][0]['PRODUCTION_YEAR'][0]['value'];
				
				// Pays de production
				$film['production_countries'] = array();
				if (is_array($result['FILM'][0]['TECHNICAL'][0]['PRODUCTION_COUNTRIES'][0]['COUNTRY']) && count($result['FILM'][0]['TECHNICAL'][0]['PRODUCTION_COUNTRIES'][0]['COUNTRY'])) {
					foreach ($result['FILM'][0]['TECHNICAL'][0]['PRODUCTION_COUNTRIES'][0]['COUNTRY'] as $country) {
						if (is_array($country['LABEL']) && count($country['LABEL'])) {
							foreach ($country['LABEL'] as $label) {
								if ($label['LANG'] == 'fr') {
									$film['production_countries'] = $label['value'];
								}
							}
						}
					}
				}
				
				// Langues
				$film['languages'] = array();
				if (is_array($result['FILM'][0]['TECHNICAL'][0]['LANGUAGES'][0]['LANGUAGE']) && count($result['FILM'][0]['TECHNICAL'][0]['LANGUAGES'][0]['LANGUAGE'])) {
					foreach ($result['FILM'][0]['TECHNICAL'][0]['LANGUAGES'][0]['LANGUAGE'] as $language) {
						if (is_array($language['LABEL']) && count($language['LABEL'])) {
							foreach ($language['LABEL'] as $label) {
								if ($label['LANG'] == 'fr') {
									$film['languages'] = $label['value'];
								}
							}
						}
					}
				}
				
				// Lien externe
				if ($result['FILM'][0]['EXTERNALURI'][0]['value']) {
					$film['externaluri'] = 'http://portal.mediatheque-numerique.com/sso_login?return_url='.url_encode($result['FILM'][0]['EXTERNALURI'][0]['value']);
					if($_SESSION['user_code'] && isset($infos['privatekey'])) {
						global $empr_cb, $empr_nom, $empr_prenom, $empr_mail, $empr_year;
						
						$id_encrypted = hash('sha256', $empr_cb.$infos['privatekey']);
						
						$film['externaluri'] .= "&sso_id=mednum&id=".$empr_cb."&email=".$empr_mail."&nom=".strtolower($empr_nom)."&prenom=".strtolower($empr_prenom)."&dnaiss=".$empr_year."-12-31&id_encrypted=".$id_encrypted;
					}
				}
				$enrichment[$type]['content'] = H2o::parseString(stripslashes($vars['enrichment_template']))->render(array("film"=>$film));
				break;
		}		
		$enrichment['source_label'] = $this->msg['artevod_enrichment_source'];
		return $enrichment;
	}
	
	public function getEnrichmentHeader($source_id){
		$header= array();
		return $header;
	}
}// class end


