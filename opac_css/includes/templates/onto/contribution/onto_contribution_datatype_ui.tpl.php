<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: onto_contribution_datatype_ui.tpl.php,v 1.4 2017-07-12 08:32:48 apetithomme Exp $

if (stristr($_SERVER['REQUEST_URI'], ".tpl.php")) die("no access");

global $ontology_tpl,$msg,$base_path;

$ontology_tpl['onto_contribution_datatype_docnum_file_script'] = '
		<script type="text/javascript">
			if (!window.!!instance_name!!_!!property_name!!_change) {
				window.!!instance_name!!_!!property_name!!_change = true;
			    require(["dojo/request/iframe", "dojo/query", "dojo/on", "dojo/dom-attr", "dojo/dom-construct", "dojo/dom", "dojo/dom-style", "dojo/ready"], function (iframe, query, on, domAttr, domConstruct, dom, domStyle, ready) {
			        ready(function () {
			            query("#!!instance_name!!_!!property_name!! input[type=\'file\']").forEach(function (node) {
			                on(node, "change", function (e) {
			                    var form_name = domAttr.get(e.target.form, "name");
			                    iframe("'.$base_path.'/ajax.php?module=ajax&categ=contribution&sub=ajax_check_values&what=docnum_file_doublon", {
			                        form: form_name,
			                        data: {
			                            field_name: domAttr.get(e.target, "name")
			                        },
			                        handleAs: "json"
			                    }).then(function (data) {
			                        if (dom.byId("docnum_file_duplications")) {
			                            domConstruct.destroy(dom.byId("docnum_file_duplications"));
			                        }
			                        if (data.doublon) {
			                            var html = "<div id=\"docnum_file_duplications\"><strong style=\"color:red;\">'.$msg['onto_contribution_datatype_docnum_file_duplication_existing'].'<\/strong><br/>";
			                            for (var i = 0; i < data.records.length; i++) {
			                                html += data.records[i];
			                            }
			                            html += "<\/div>";
			                            domConstruct.place(html, e.target, "after");
			                            domStyle.set(form_name + "_onto_contribution_save_button", "display", "none");
			                            domStyle.set(form_name + "_onto_contribution_push_button", "display", "none");
			                        } else {
			                            domStyle.set(form_name + "_onto_contribution_save_button", "display", "");
			                            domStyle.set(form_name + "_onto_contribution_push_button", "display", "");
			                            var labelNode = dom.byId(node.getAttribute("id").replace("docnum_file", "label"));
			                           	if(labelNode && !labelNode.value){
			                            	if(node.value.lastIndexOf("\\\") != -1){
			                            		labelNode.value = node.value.substr(node.value.lastIndexOf("\\\")+1);
			                            	}else{
			                            		labelNode.value = node.value;
			                            	}
			                            	 
			                           	}
			                        }
			                    }, function (err) {
			                        console.log(err);
			                    });
			                });
			            });
			        });
			    });
			}
		</script>';