<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: contribution_area_check_values.inc.php,v 1.2.2.1 2017-09-14 14:45:28 tsamson Exp $

if (stristr($_SERVER['REQUEST_URI'], ".inc.php")) die("no access");

if (!$opac_contribution_area_activate || !$allow_contribution) {
	die();
}

require_once($class_path.'/encoding_normalize.class.php');
require_once($class_path.'/record_display.class.php');

$field_elements = explode('[', $field_name);
if (isset($_FILES[$field_elements[0]])) {
	$explnum_tmp_name = $_FILES[$field_elements[0]]['tmp_name'];
	for ($i = 1; $i < count($field_elements); $i++) {
		$explnum_tmp_name = $explnum_tmp_name[rtrim($field_elements[$i], "]")];
	}
	$explnum_signature = md5_file($explnum_tmp_name);

	$return = array('doublon' => 0);
	print '<textarea>';
	if ($explnum_signature) {
		$result = pmb_mysql_query('select explnum_notice, explnum_bulletin from explnum where explnum_signature = "'.$explnum_signature.'"');
		if (pmb_mysql_num_rows($result)) {
			$permalinks = array();
			while($row = pmb_mysql_fetch_object($result)) {
				$rights = record_display::get_record_rights($row->explnum_notice, $row->explnum_bulletin);
				if ($rights['visible']) {
					$permalinks[] = record_display::get_display_isbd_with_link($row->explnum_notice, $row->explnum_bulletin);
				}
			}
			$return = array('doublon' => 1, 'records' => $permalinks);
		}
	}
	print encoding_normalize::json_encode($return);
	print '</textarea>';
}