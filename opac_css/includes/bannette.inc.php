<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: bannette.inc.php,v 1.12.2.1 2017-10-18 13:59:37 plmrozowski Exp $

if (stristr($_SERVER['REQUEST_URI'], ".inc.php")) die("no access");

if(!isset($id_bannette)) $id_bannette = 0;
if(!isset($date_diff)) $date_diff = '';

// affichage du contenu d'une bannette
require_once($base_path."/includes/bannette_func.inc.php");

// afin de r�soudre un pb d'effacement de la variable $id_empr par empr_included, bug � trouver
if (!$id_empr) $id_empr=$_SESSION["id_empr_session"] ;
print "<script type='text/javascript' src='./includes/javascript/tablist.js'></script>" ;
print "<div id='aut_details' class='aut_details_bannette'>\n";

if ($id_bannette){
	print "<h3><span>".affiche_nom_bannette($id_bannette)."</span></h3><br />";
	$aff = pmb_bidi(affiche_bannette ("$id_bannette", 0, $opac_bannette_notices_format, $opac_bannette_notices_depliables, "./empr.php?lvl=bannette&id_bannette=!!id_bannette!!", $liens_opac ,$date_diff)) ; 
}else{ 
	print "<h3><span>".$msg['accueil_bannette']."</span></h3><br />";
	$aff = pmb_bidi(affiche_bannette ("", $opac_bannette_nb_liste, $opac_bannette_notices_format, $opac_bannette_notices_depliables, "./empr.php?lvl=bannette&id_bannette=!!id_bannette!!", $liens_opac ,$date_diff )) ;
}

if($aff){
	if ($opac_bannette_notices_depliables) print $begin_result_liste ;
	print $aff;
} else {
	print $msg['empr_no_alerts'];
}
print "</div><!-- fermeture #aut_see -->\n";	
?>