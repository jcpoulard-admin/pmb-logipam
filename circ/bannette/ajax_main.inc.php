<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: ajax_main.inc.php,v 1.1 2016-08-08 14:31:38 ngantier Exp $

if (stristr($_SERVER['REQUEST_URI'], ".inc.php")) die("no access");

require_once("$class_path/emprunteur.class.php");

$empr=new emprunteur($empr_id);

switch($sub){
	case 'get_form':
		ajax_http_send_response($form = $empr->get_bannette_form());
		break;
	case 'save_abon':
		ajax_http_send_response($empr->save_bannette_abon($bannette_abon)); 
		break;
	case 'delete_abon':
		ajax_http_send_response($empr->delete_bannette_abon($bannette_abon));		
		break;				
	default:
		ajax_http_send_error('400',$msg["ajax_commande_inconnue"]);
		break;		
}	
