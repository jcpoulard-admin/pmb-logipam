<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: visits_statistics.inc.php,v 1.1 2017-01-10 08:12:09 apetithomme Exp $
if (stristr($_SERVER['REQUEST_URI'], ".inc.php")) die("no access");

if (!$empr_visits_statistics_active) die();

require_once($class_path.'/visits_statistics.class.php');

$visits_statistics = new visits_statistics();

switch ($sub) {
	case 'update':
		$visits_statistics->update_statistics($counter_type, $value);
		break;
	case 'get_data':
		print $visits_statistics->get_json_statistics();
		break;
}