$(document).ready(function() {
	//$("#contenu>div[id$='Child']").wrap("<div class='row empr-section'></div>");
	$("#contenu>table").wrap("<div class='row table-container bkg-white'></div>");
	$("#contenu>input,#contenu>p,#contenu>ul,#contenu>div[id$='Child']").wrap("<div class='row'></div>");
	$('#contenu>*:not(.row):has(>.left+.right)').children().wrapAll("<div class='row bkg-grey'></div>");
	$('.item-expand').nextUntil("div").wrapAll("<span class='actions-search'></span>'");
	$('.actions-search,.item-expand,#contenu>b').wrapAll( "<div class='row'></div>" );
	$('#search_fields_tree_collapseall,#search_fields_tree_expandall').wrapAll( "<div class='row'></div>" );
	$(".admin #contenu>.row>.row>.item-expand>a").addClass("ss-nav-cms-item-expand ");
	$("#contenu-frame .form-alter> a").addClass("bdd_updade");//chargment ajax a modifier 
	$("#cms_drag_activate_button").parent(".row").addClass("ui-cms-drag-button");
	$(".cms div[onclick*='expandBase']").parent(".row").addClass("ui-expand-container");
	//$("#cms_build_navig_informations").next().css("height","675px"); ne fonctionne pas :(
	
	
	// search result
    if ( $('#contenu>div').has(".actions-search")){
		$('.notice-parent,.notice-child').addClass('search-result-item')
	}; 
	//----]	
    if (document.URL.indexOf("categ=search") != -1) {
        $('.notice-parent,.notice-child').wrapAll("<div class='row'><ul class='uk-list uk-list-striped'></ul></div>");
		var notParent = Array.prototype.slice.call(document.querySelectorAll('.uk-list-striped .notice-parent'));
		notParent.forEach(function(parent){
			var li = document.createElement('li');
			//$(li).
			parent.parentElement.appendChild(li);
			var child = parent.nextElementSibling;
			li.appendChild(parent);
			li.appendChild(child);
			var parentElt = parent.parentElement;
		})	
		$("div:not('.notice-child')").each(function(){
			if ($(this).children().length == 0) {
				$(this).addClass('wyr-empty-box')
			}
		});
    };
    if (document.URL.indexOf("categ=last_records") != -1) {
        $('.notice-parent,.notice-child').wrapAll("<div class='row'><ul class='uk-list uk-list-striped'></ul></div>");
		var notParent = Array.prototype.slice.call(document.querySelectorAll('.uk-list-striped .notice-parent'));
		notParent.forEach(function(parent){
			var li = document.createElement('li');
			//$(li).
			parent.parentElement.appendChild(li);
			var child = parent.nextElementSibling;
			li.appendChild(parent);
			li.appendChild(child);
			var parentElt = parent.parentElement;
		})	
		$("div:not('.notice-child')").each(function(){
			if ($(this).children().length == 0) {
				$(this).addClass('wyr-empty-box')
			}
		});
    };	
	if (document.URL.indexOf("categ=caddie") != -1) {
		$('#contenu>.notice-child,#contenu>.notice-parent').wrapAll(("<div class='row'><div class='row'></div></div>"));
	};				
	$('.row.wyr-empty-box').next("div[class^='colonne']").wrap("<div class='row'></div>");
	
	$("#saisie_auteur.form-autorites").prepend("<div class='row bkg-white recepter'></div>");
	var moveItem = $("#saisie_auteur.form-autorites>.row>div.left,#saisie_auteur.form-autorites>.row>div.right").detach();
	$(".recepter").prepend(moveItem);
	// notication #extra2
	if ($(".notification>img[src$='new.png']").length == 1){
		$(".notification").addClass("uk-active")
	};
	
});