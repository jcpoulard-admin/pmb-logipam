<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\models\LogipamVisiteur; 
$model  = new LogipamVisiteur(); 
$gender = 1; 
if($code == 'V100F'){
    $gender = 2;
}elseif($code == 'V100M'){
    $gender = 1; 
}
$model->gender = $gender; 
$model->date_presence = date('Y-m-d');
$model->time = date('H:i:s');
$model->location = 12; 
$model->save();

?>
<div class="alert alert-success">
    <h3><i class="icon fa fa-thumbs-up"></i>Visiteur enregistr&eacute; avec succ&egrave;s !</h3>
</div>