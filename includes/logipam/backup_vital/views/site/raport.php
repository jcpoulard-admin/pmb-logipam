<?php
use app\models\LogipamPresence; 
// Requete Feminin
    $sql_str_female = "SELECT lp.id, lp.id_empr, lp.date_presence, lp.day_presence FROM logipam_presence lp INNER JOIN empr e ON (e.id_empr = lp.id_empr) WHERE e.empr_sexe = 2"; 
    $data_female = LogipamPresence::findBySql($sql_str_female)->asArray()->all(); 
    // Total visite par jour  
    $dim_f = 0;
    $lun_f = 0;
    $mar_f = 0;
    $mer_f = 0;
    $jeu_f = 0;
    $ven_f = 0;
    $sam_f = 0;
    // Initialisation moyenne 
    $dim_af = 0;
    $lun_af = 0;
    $mar_af = 0;
    $mer_af = 0;
    $jeu_af = 0;
    $ven_af = 0;
    $sam_af = 0;
    
    $average_f = [];
    
    $presence = new LogipamPresence(); 
    
    for($i=0; $i < sizeof($data_female); $i++){
        
        switch ($data_female[$i]['day_presence']){
            case 0 :
                $dim_f++;
                break;
            case 1: 
                $lun_f++;
                break;
            case 2: 
                $mar_f++;
                break;
            case 3: 
                $mer_f++;
                break;
            case 4: 
                $jeu_f++;
                break;
            case 5: 
                $ven_f++;
                break;
            case 6: 
                $sam_f++;
                break;
            }
        }
   
    
   if($presence->getCountDay(0)!=0){
       $dim_af = $dim_f/$presence->getCountDay(0); 
   } 
   if($presence->getCountDay(1)!=0){
       $lun_af = $lun_f/$presence->getCountDay(1); 
   } 
   if($presence->getCountDay(2)!=0){
       $mar_af = $mar_f/$presence->getCountDay(2); 
   }
   if($presence->getCountDay(3)!=0){
       $mer_af = $mer_f/$presence->getCountDay(3); 
   } 
   if($presence->getCountDay(4)!=0){
       $jeu_af = $jeu_f/$presence->getCountDay(4); 
   } 
   if($presence->getCountDay(5)!=0){
       $ven_af = $ven_f/$presence->getCountDay(5); 
   } 
   if($presence->getCountDay(6)!=0){
       $sam_af = $sam_f/$presence->getCountDay(6); 
   } 
        
// Requete masculin
    $sql_str_male = "SELECT lp.id, lp.id_empr, lp.date_presence, lp.day_presence FROM logipam_presence lp INNER JOIN empr e ON (e.id_empr = lp.id_empr) WHERE e.empr_sexe = 1"; 
    $data_male = LogipamPresence::findBySql($sql_str_male)->asArray()->all(); 
      
    $dim_m = 0;
    $lun_m = 0;
    $mar_m = 0;
    $mer_m = 0;
    $jeu_m = 0;
    $ven_m = 0;
    $sam_m = 0;
    
    // Initialisation moyenne 
    $dim_am = 0;
    $lun_am = 0;
    $mar_am = 0;
    $mer_am = 0;
    $jeu_am = 0;
    $ven_am = 0;
    $sam_am = 0;
   
    
    for($i=0; $i < sizeof($data_male); $i++){
        
        switch ($data_male[$i]['day_presence']){
            case 0 :{
                $dim_m++; 
                
                }
                break;
            case 1: 
                $lun_m++;
                break;
            case 2: 
                $mar_m++;
                break;
            case 3: 
                $mer_m++;
                break;
            case 4: 
                $jeu_m++;
                break;
            case 5: 
                $ven_m++;
                break;
            case 6: 
                $sam_m++;
                break;
            }
        } 
        
    if($presence->getCountDay(0)!=0){
       $dim_am = $dim_m/$presence->getCountDay(0); 
   } 
   if($presence->getCountDay(1)!=0){
       $lun_am = $lun_m/$presence->getCountDay(1); 
   } 
   if($presence->getCountDay(2)!=0){
       $mar_am = $mar_m/$presence->getCountDay(2); 
   }
   if($presence->getCountDay(3)!=0){
       $mer_am = $mer_m/$presence->getCountDay(3); 
   } 
   if($presence->getCountDay(4)!=0){
       $jeu_am = $jeu_m/$presence->getCountDay(4); 
   } 
   if($presence->getCountDay(5)!=0){
       $ven_am = $ven_m/$presence->getCountDay(5); 
   } 
   if($presence->getCountDay(6)!=0){
       $sam_am = $sam_m/$presence->getCountDay(6); 
   }     
     
  $moyenne_feminin = []; 
  $moyenne_masculin = [];
  // Affecter la moyenne par mois pour feminin
  for($i=1;$i<=12;$i++){
      if($presence->getCountMonth($i)!=0){
          $moyenne_feminin[$i] = $presence->getPresenceByMonthGender($i, 2);
      }else{
          $moyenne_feminin[$i] = 0;
      } 
  }
  // Affecter la moyenne par  mois pour masculin 
  for($i=1;$i<=12;$i++){
      if($presence->getCountMonth($i)!=0){
          $moyenne_masculin[$i] = $presence->getPresenceByMonthGender($i, 1);
      }else{
          $moyenne_masculin[$i] = 0;
      } 
  }

?>


<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Moyenne pr&eacute;sence par sexe et par jour de la semaine</h3>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
            <div id="js-legend" class="chart-legend"></div>
</div>

 <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Moyenne pr&eacute;sence par sexe et par mois</h3>

              
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
            <div id="js-legend2" class="chart-legend"></div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        /* ChartJS
     * ------- 'rgba(210, 214, 222, 1)',
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)

    var areaChartData = {
      labels  : ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
      datasets: [
        {
          label               : 'F&eacute;minin',
          fillColor           : 'rgba(0, 166, 0, 1)',
          strokeColor         : 'rgba(0, 166, 0, 1)',
          pointColor          : 'rgba(0, 166, 0, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [<?= round($lun_af,0) ?>, <?= round($mar_af,0) ?>, <?= round($mer_af,0) ?>, <?= round($jeu_af,0) ?>, <?= round($ven_af,0) ?>, <?= round($sam_af,0) ?>]
        },
        {
          label               : 'Masculin',
          fillColor           : 'rgba(243,156,18,0.5)',
          strokeColor         : 'rgba(243,156,18,0.8)',
          pointColor          : 'rgba(243,156,18,0.8)',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#ff0',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?= round($lun_am,0) ?>, <?= round($mar_am,0) ?>, <?= round($mer_am,0) ?>, <?= round($jeu_am,0) ?>, <?= round($ven_am,0) ?>, <?= round($sam_am,0) ?>]
        }
      ],
      options: {
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        }
    }
    }
    

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,//false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : true,//false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);
    
    document.getElementById('js-legend').innerHTML =  "<table><tr><td><span style='color :#F39C12'>Masculin</span></td></tr><tr><td><span style='color :#00A65A'>F&eacute;minin</span></td></tr></table>";
    
    
    //-------------
    //- BAR CHART -
    //-------------
    
    var barChartData = {
      labels  : ['Janvier', 'F\351vrier', 'Mars', 'Avril', 'Mai', 'Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
      datasets: [
        {
          label               : 'F&eacute;minin',
          fillColor           : 'rgba(0, 166, 0, 1)',
          strokeColor         : 'rgba(0, 166, 0, 1)',
          pointColor          : 'rgba(0, 166, 0, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [<?= '"'.implode('","', $moyenne_feminin).'"' ?>]
        },
        {
          label               : 'Masculin',
          fillColor           : 'rgba(243,156,18,0.5)',
          strokeColor         : 'rgba(243,156,18,0.8)',
          pointColor          : 'rgba(243,156,18,0.8)',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#ff0',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?= '"'.implode('","', $moyenne_masculin).'"' ?>]
        }
      ],
      options: {
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        }
    }
    }
    
    
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d');
    var barChart                         = new Chart(barChartCanvas);
    var barChartData                     = barChartData;
    barChartData.datasets[1].fillColor   = '#F39C12';//'#00a65a';
    //barChartData.datasets[1].strokeColor = '#00a65a';
    barChartData.datasets[1].pointColor  = '#00a65a';
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
    document.getElementById('js-legend2').innerHTML =  "<table><tr><td><span style='color :#F39C12'>Masculin</span></td></tr><tr><td><span style='color :#00A65A'>F&eacute;minin</span></td></tr></table>";
    
    });
  
</script>



