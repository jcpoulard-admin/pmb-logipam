<div class="col-lg-10" id='an-tete'>
    
     <div class="row zon-dachbod" style="padding: 8px;">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
                <h3><span id="total_visit"></span></h3>

                <p>Total visite adh&eacute;rent</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
         
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
                <h3><span id='total_female'></span></h3>

              <p>F&eacute;minin</p>
            </div>
            <div class="icon">
              <i class="fa fa-venus"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
                <h3><span id='total_male'></span></h3>

              <p>Masculin</p>
            </div>
            <div class="icon">
              <i class="fa fa-mars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
                <h3><span id="adhesion_depassee"></span></h3>

              <p>
                  Total Visiteur
                  <i class="fa fa-male"></i> : <span id="v-male"></span> <i class="fa fa-female"></i> : <span id="v-female"></span>
              </p>
              
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
      </div>
    
    <div class="input-group input-group-sm zon-sezi">
        <input class="form-control" id="empr_cb" type="text" placeholder="Saisir le code du lecteur">
            <span class="input-group-btn">
                <button type="button" id="btnSave" class="btn btn-info btn-flat"><i class="fa fa-save"></i> Enregistrer</button>
            </span>
    </div>
    <p>
        
    </p>
    
    <div id="lekte">
    
    </div>
   
    <!--
    <div id="heure">
        
    </div>
    -->
</div>
<div id='upload-foto'>
    
</div>
<div id="zon-kat-enprime">
    <p><label>Recherche carte d&eacute;j&agrave; imprim&eacute;e</label></p>
    <input type="text" id="idSearch" placeholder="Rechercher" />
    <div id="lis-enprime">
        
    </div>
</div>


<div class="col-lg-2">
    
</div>

<script type="text/javascript">
    
    
	$(document).ready(function(){
            $("#empr_cb").focus();
            heure();
            totalVisite();
            totalFemaleVisite();
            totalMaleVisite();
            totalVisiteur();
            totalVisiteurM();
            totalVisiteurF();
            $("#zon-kat-enprime").hide();
            
            // Afficher le rapport en dessous 
            var valeur = 1; 
            $.get('includes/logipam/yii/web/index.php?r=site/get-rapport-visite',{valeur : valeur},function(data){
            
                $('#lekte').html(data);
            });
            
        });
        
   $("#btnSave").click(function(){
            var valeur = $("#empr_cb").val();
            $.get('includes/logipam/yii/web/index.php?r=site/get-lecteur-info',{code : valeur},function(data){
            $('#lekte').html(data);
            
            });
            totalVisite(); 
            totalFemaleVisite();
            totalMaleVisite();
            totalVisiteur(); 
            totalVisiteurM();
            totalVisiteurF();
            $("#empr_cb").val("");
            $("#empr_cb").focus();
        });
        
    $(document).keypress(function(e) {
        if(e.which === 13) {
            var valeur = $("#empr_cb").val();
            $.get('includes/logipam/yii/web/index.php?r=site/get-lecteur-info',{code : valeur},function(data){
            $('#lekte').html(data);
            
            });
            totalVisite(); 
            totalFemaleVisite();
            totalMaleVisite();
            totalVisiteur(); 
            totalVisiteurM();
            totalVisiteurF();
            $("#empr_cb").val("");
            $("#empr_cb").focus();
        }
    });
        
        function heure(){
            var valeur = 1;
            $.get('includes/logipam/yii/web/index.php?r=site/get-heure',{code : valeur},function(data){
            $('#heure').html(data);
            setTimeout(heure, 10000);
            });
        }
        
        function totalVisite(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-total-visite',{},function(data){
            $('#total_visit').html(data);
            setTimeout(totalVisite, 10000);
            });
        }
        
        function totalFemaleVisite(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-total-female-visite',{},function(data){
            $('#total_female').html(data);
            setTimeout(totalFemaleVisite, 10000);
            });
        }
        
        function totalMaleVisite(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-total-male-visite',{},function(data){
            $('#total_male').html(data);
            setTimeout(totalMaleVisite, 10000);
            });
        }
        /*
        function adhesionDepassee(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-adhesion-depassee',{},function(data){
            $('#adhesion_depassee').html(data);
            setTimeout(adhesionDepassee, 10000);
            });
        }
        */
       function totalVisiteur(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-total-visitor',{},function(data){
            $('#adhesion_depassee').html(data);
            setTimeout(totalVisiteur, 10000);
            });
        }
        
        function totalVisiteurM(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-total-visitor-sexe',{sexe:1},function(data){
            $('#v-male').html(data);
            setTimeout(totalVisiteurM, 10000);
            });
        }
        
        function totalVisiteurF(){
            $.get('includes/logipam/yii/web/index.php?r=site/get-total-visitor-sexe',{sexe:2},function(data){
            $('#v-female').html(data);
            setTimeout(totalVisiteurF, 10000);
            });
        }
       
        $("#takePrezans").click(function(){
            $('#upload-foto').hide();
            $("#an-tete").show();
            $("#lekte").empty();
            $("#empr_cb").val("");
            $("#empr_cb").focus(); 
            totalVisite();
            totalFemaleVisite();
            totalMaleVisite();
            totalVisiteur();
            totalVisiteurM();
            totalVisiteurF();
            $(".zon-dachbod").show();
            $(".zon-sezi").show();
            $("#zon-kat-enprime").hide();
            
            // Afficher le rapport en dessous 
            var valeur = 1; 
            $.get('includes/logipam/yii/web/index.php?r=site/get-rapport-visite',{valeur : valeur},function(data){
            
                $('#lekte').html(data);
            });
        });
        
        $("#rapo").click(function(){
            $('#upload-foto').hide();
            $("#an-tete").show();
            $('#lekte').empty();
            $(".zon-dachbod").show();
            $(".zon-sezi").show();
            $("#zon-kat-enprime").hide();
            
            var valeur = 1; 
            $.get('includes/logipam/yii/web/index.php?r=site/get-rapport-visite',{valeur : valeur},function(data){
            
                $('#lekte').html(data);
            });
        totalVisite();
        
        });
        
        $("#liste").click(function(){
            $("#an-tete").show();
            $('#lekte').empty();
            $('#upload-foto').hide();
            $(".zon-dachbod").hide();
            $(".zon-sezi").hide();
            $("#zon-kat-enprime").hide();
            var valeur = 1; 
            $.get('includes/logipam/yii/web/index.php?r=site/get-rapport-liste',{valeur : valeur},function(data){
            
            $('#lekte').html(data);
        });
        totalVisite();
        
        });
        
        $("#addPhoto").click(function(){
            $("#an-tete").hide();
            $('#upload-foto').show();
            $("#zon-kat-enprime").hide();
            $.get('includes/logipam/yii/web/index.php?r=site/upload',{},function(data){
                $('#upload-foto').html(data);
            });
        });
        
        $("#cartePrint").click(function(){
            
            $("#lis-lekte").hide();
            $("#an-tete").hide();
            $('#upload-foto').hide();
            $("#zon-kat-enprime").show();
          
          // var valeur = $("#idSearch").val();
          var valeur = null;
            $.get('includes/logipam/yii/web/index.php?r=site/get-lis-enprime',{value: valeur},function(data){
                $('#lis-enprime').html(data);
            });
        });
        
        $("#newCard").click(function(){
            
            $("#lis-lekte").hide();
            $("#an-tete").hide();
            $('#upload-foto').hide();
            $("#zon-kat-enprime").show();
          
          // var valeur = $("#idSearch").val();
          var valeur = null;
            $.get('includes/logipam/yii/web/index.php?r=site/get-new-lis-enprime',{value: valeur},function(data){
                $('#lis-enprime').html(data);
            });
        });
        
        $("#idSearch").keyup(function(){
            
            var valeur = $("#idSearch").val();
            $.get('includes/logipam/yii/web/index.php?r=site/get-lis-enprime',{value : valeur},function(data){
            
                    $('#lis-enprime').html(data);
            //setTimeout(tablo, 10000);
            });
            
        });
        
        
        
</script>


