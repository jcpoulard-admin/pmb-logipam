<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collections".
 *
 * @property int $collection_id
 * @property string $collection_name
 * @property int $collection_parent
 * @property string $collection_issn
 * @property string $index_coll
 * @property string $collection_web
 * @property string $collection_comment
 * @property int $authority_import_denied
 */
class Collections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collection_parent', 'authority_import_denied'], 'integer'],
            [['index_coll', 'collection_web', 'collection_comment'], 'string'],
            [['collection_web', 'collection_comment'], 'required'],
            [['collection_name'], 'string', 'max' => 255],
            [['collection_issn'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'collection_id' => 'Collection ID',
            'collection_name' => 'Collection Name',
            'collection_parent' => 'Collection Parent',
            'collection_issn' => 'Collection Issn',
            'index_coll' => 'Index Coll',
            'collection_web' => 'Collection Web',
            'collection_comment' => 'Collection Comment',
            'authority_import_denied' => 'Authority Import Denied',
        ];
    }
}
