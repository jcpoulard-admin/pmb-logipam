<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "docs_location".
 *
 * @property int $idlocation
 * @property string $location_libelle
 * @property string $locdoc_codage_import
 * @property int $locdoc_owner
 * @property string $location_pic
 * @property int $location_visible_opac
 * @property string $name
 * @property string $adr1
 * @property string $adr2
 * @property string $cp
 * @property string $town
 * @property string $state
 * @property string $country
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $logo
 * @property string $commentaire
 * @property int $transfert_ordre
 * @property int $transfert_statut_defaut
 * @property int $num_infopage
 * @property string $css_style
 * @property int $surloc_num
 * @property int $surloc_used
 * @property int $show_a2z
 */
class DocsLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docs_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locdoc_owner', 'location_visible_opac', 'transfert_ordre', 'transfert_statut_defaut', 'num_infopage', 'surloc_num', 'surloc_used', 'show_a2z'], 'integer'],
            [['commentaire'], 'required'],
            [['commentaire'], 'string'],
            [['location_libelle', 'locdoc_codage_import', 'location_pic', 'name', 'adr1', 'adr2', 'logo'], 'string', 'max' => 255],
            [['cp'], 'string', 'max' => 15],
            [['town', 'state', 'country', 'phone', 'email', 'website', 'css_style'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idlocation' => 'Idlocation',
            'location_libelle' => 'Location Libelle',
            'locdoc_codage_import' => 'Locdoc Codage Import',
            'locdoc_owner' => 'Locdoc Owner',
            'location_pic' => 'Location Pic',
            'location_visible_opac' => 'Location Visible Opac',
            'name' => 'Name',
            'adr1' => 'Adr1',
            'adr2' => 'Adr2',
            'cp' => 'Cp',
            'town' => 'Town',
            'state' => 'State',
            'country' => 'Country',
            'phone' => 'Phone',
            'email' => 'Email',
            'website' => 'Website',
            'logo' => 'Logo',
            'commentaire' => 'Commentaire',
            'transfert_ordre' => 'Transfert Ordre',
            'transfert_statut_defaut' => 'Transfert Statut Defaut',
            'num_infopage' => 'Num Infopage',
            'css_style' => 'Css Style',
            'surloc_num' => 'Surloc Num',
            'surloc_used' => 'Surloc Used',
            'show_a2z' => 'Show A2z',
        ];
    }
}
