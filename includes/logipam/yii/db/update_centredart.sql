/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Jul 30, 2018
 */

-- Table pour enregistrer la liste des mots cles 
CREATE TABLE `notices_keywords` ( `id` INT NOT NULL AUTO_INCREMENT , `mots_cles` VARCHAR(255) NOT NULL , `creer_par` VARCHAR(64) NULL , `date_creation` DATETIME NULL , `modifie_par` VARCHAR(64) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`), UNIQUE (`mots_cles`)) ENGINE = InnoDB;

-- Table pour enregistrer les autres types de personnes en interaction avec les oeuves 
CREATE TABLE `authors_oeuvre` ( `id` INT NOT NULL AUTO_INCREMENT , `prenom` VARCHAR(255) NULL , `nom` VARCHAR(255) NULL , `date_person` VARCHAR(64) NULL , `index_person` TEXT NULL , `type` INT NULL COMMENT '1: Personne lie a l acquisition; 2: Acheteur; 3:Nom restaurateur' , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifie_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`prenom`), INDEX (`nom`)) ENGINE = InnoDB;

-- Table enregistrement mots cles des notices 
CREATE TABLE `oeuvre_motcles` ( `id` INT NOT NULL AUTO_INCREMENT , `notice_id` INT NOT NULL , `mots_cles` TEXT NULL , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifie_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table enrgistrement des infos de provenance de l'oeuvre 
CREATE TABLE `oeuvre_provenance` ( `id` INT NOT NULL AUTO_INCREMENT , `notice_id` INT NOT NULL , `type_acquisition` VARCHAR(255) NULL , `person_acquisition` INT NULL , `date_acquisition` DATE NULL , `prix_achat` FLOAT NULL , `valeur_assurance` FLOAT NULL , `vente` TEXT NULL , `date_vente` DATETIME NULL , `acheteur` INT NULL , `prix_vente` FLOAT NULL , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifier_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table enregistrement des informations physique sur l'oeuvre 
CREATE TABLE `oeuvre_physique` ( `id` INT NOT NULL AUTO_INCREMENT , `notice_id` INT NOT NULL , `localisation` VARCHAR(255) NULL , `emplacement` VARCHAR(255) NULL , `etat_conservation` VARCHAR(255) NULL , `constat` TEXT NULL , `nom_restaurateur` INT NULL , `date_restauration` DATETIME NULL , `object_restauration` TEXT NULL , `cree_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `modifier_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`notice_id`)) ENGINE = InnoDB;

-- Table enregistrement d'autres informations de l'oeuvre 
CREATE TABLE `notices_other_data` ( `id` INT NOT NULL AUTO_INCREMENT , `notices_id` INT NOT NULL , `lieu_creation` VARCHAR(255) NULL , `longue` FLOAT NULL , `larg` FLOAT NULL , `haut` FLOAT NULL , `materiaux` VARCHAR(255) NULL , `technique` VARCHAR(255) NULL , `description` TEXT NULL , `legende_image` VARCHAR(255) NULL , `creer_par` VARCHAR(64) NULL , `date_creation` DATETIME NULL , `modifier_par` VARCHAR(64) NULL , `date_modification` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; 

-- Table enregistrement des localisations des oeuvres
CREATE TABLE `oeuvre_localisation` ( `id` INT NOT NULL AUTO_INCREMENT , `nom_localisation` VARCHAR(255) NOT NULL , `cree_par` VARCHAR(255) NULL , `modifie_par` VARCHAR(255) NULL , `date_creation` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; 

-- Modifier la table notices_custom_values 
ALTER TABLE `notices_custom_values` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);

-- 09/10/2018
-- Table pour enregistrer les musees 
CREATE TABLE `collection_musee` ( `id` INT NOT NULL AUTO_INCREMENT , `nom_musee` VARCHAR(255) NOT NULL , `adresse_musee` VARCHAR(255) NULL , `phone_musee` VARCHAR(32) NULL , `date_creation` DATETIME NULL , `cree_par` VARCHAR(64) NULL , `date_modification` DATETIME NULL , `modifier_par` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`nom_musee`)) ENGINE = InnoDB;
ALTER TABLE `collection_musee` ADD `prenom_contact` VARCHAR(255) NULL AFTER `phone_musee`, ADD `nom_contact` VARCHAR(255) NULL AFTER `prenom_contact`, ADD `phone_contact` VARCHAR(64) NULL AFTER `nom_contact`;

-- Table poru enregistrer les prets et les retours 
CREATE TABLE `collection_pret` ( `id` INT NOT NULL AUTO_INCREMENT , `musee_id` INT NOT NULL , `titre_expo` VARCHAR(255) NOT NULL , `date_pret` DATETIME NOT NULL , `date_retour_prog` DATETIME NULL , `notice_id` INT NOT NULL , `contact` VARCHAR(255) NOT NULL , `is_retour` BOOLEAN NULL , `date_retour` DATETIME NULL , `date_creation` DATETIME NULL , `cree_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , `modifier_par` VARCHAR(255) NULL , PRIMARY KEY (`id`), INDEX (`musee_id`)) ENGINE = InnoDB;

-- Tbale pour enregistrer les emprunteurs des oeuvres 
CREATE TABLE `collection_emprunteur` ( `id` INT NOT NULL AUTO_INCREMENT , `prenom_empr` VARCHAR(255) NOT NULL , `nom_empr` VARCHAR(255) NOT NULL , `phone_empr` VARCHAR(64) NULL , `adresse_empr` VARCHAR(255) NULL , `date_creation` DATETIME NULL , `cree_par` VARCHAR(255) NULL , `date_modification` DATETIME NULL , `modifie_par` VARCHAR(255) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Modifier table pret 
ALTER TABLE `collection_pret` ADD `trasact_id` INT NULL AFTER `id`, ADD INDEX (`trasact_id`);
ALTER TABLE `collection_pret` CHANGE `trasact_id` `transact_id` INT(11) NULL DEFAULT NULL;

-- Migrate une partie des donnees vers le module collection 

INSERT INTO  `oeuvre_motcles`(notice_id) SELECT notice_id FROM notices;
INSERT INTO  `oeuvre_provenance`(notice_id) SELECT notice_id FROM notices;
INSERT INTO  oeuvre_physique (notice_id) SELECT notice_id FROM notices;
INSERT INTO notices_other_data (notices_id) SELECT notice_id FROM notices;

-- Ajouter des index pour optimiser la recherche 
ALTER TABLE notices ENGINE = InnoDB;
ALTER TABLE `notices_other_data` ADD INDEX `fk_notices_id` (`notices_id`);
ALTER TABLE `notices_other_data` CHANGE `notices_id` `notices_id` MEDIUMINT(8) UNSIGNED NOT NULL;
ALTER TABLE `notices_other_data` ADD FOREIGN KEY (`notices_id`) REFERENCES `notices`(`notice_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `notices_other_data` ADD INDEX (`lieu_creation`);
ALTER TABLE `notices_other_data` ADD FULLTEXT (`description`);

ALTER TABLE `oeuvre_motcles` CHANGE `notice_id` `notice_id` MEDIUMINT(8) UNSIGNED NOT NULL;
ALTER TABLE `oeuvre_motcles` ADD FOREIGN KEY (`notice_id`) REFERENCES `notices`(`notice_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `oeuvre_motcles` ADD FULLTEXT (`mots_cles`);

ALTER TABLE `oeuvre_physique` CHANGE `notice_id` `notice_id` MEDIUMINT(8) UNSIGNED NOT NULL;
ALTER TABLE `oeuvre_physique` ADD FULLTEXT (`constat`);
ALTER TABLE `oeuvre_physique` ADD FOREIGN KEY (`notice_id`) REFERENCES `notices`(`notice_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `oeuvre_physique` ADD INDEX (`etat_conservation`);

ALTER TABLE `oeuvre_provenance` CHANGE `notice_id` `notice_id` MEDIUMINT(8) UNSIGNED NOT NULL;
ALTER TABLE `oeuvre_provenance` ADD FOREIGN KEY (`notice_id`) REFERENCES `notices`(`notice_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `notices` ADD FULLTEXT (`tit1`);

