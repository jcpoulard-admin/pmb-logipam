/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Oct 21, 2019
 */

-- 21 octobre 2019 
ALTER TABLE `logipam_presence` ADD `location` INT NULL AFTER `day_presence`, ADD INDEX (`location`);

-- Simulation 
ALTER TABLE `logipam_carte_cckd` ADD `location` INT NULL AFTER `date_print` ;

UPDATE logipam_carte_cckd SET image_name = CONCAT("02",image_name);

INSERT INTO logipam_carte_membre(empr_cb, prenom, nom, sexe, date_adhesion, date_expiration,image_name, date_ajout, is_print, date_print,location) SELECT empr_cb, prenom, nom, sexe, date_adhesion, date_expiration,image_name, date_ajout, is_print, date_print,location FROM logipam_carte_cckd
