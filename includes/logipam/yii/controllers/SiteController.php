<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\LogipamPresence; 
use app\models\Empr; 
use app\models\LogipamCarteMembre; 
use kartik\mpdf\Pdf; // For PDF 
use app\models\Users; 

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "layout_pmb";
        //return $this->render('pmb');
        $current_user = $this->getUsername();
        return  $this->render('dashboard',['current_user'=>$current_user]);
    }
    /**
    public function actionIndex()
    {
        $this->layout = "layout_pmb";
        return $this->render('pmb');
    }
     * 
     */

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    public function actionGetLecteurInfo($code){
    if($code=='V100F' || $code == 'V100M'){
        $user_name = $this->getUsername(); 
        $user_data = Users::findOne(['username'=>$user_name]);  //
        $default_location = null; 
        if(!empty($user_data)){
            $default_location = $user_data['deflt_docs_location']; 
        }else{
            $default_location = null; 
        }
        echo $this->renderAjax('visitor',['code'=>$code,'username'=>$user_name,'default_location'=>$default_location]); 
    }
    else{
       $count_lecteur = Empr::find()->where(['=','empr_cb',$code])->count(); 
       if($count_lecteur==0){
           echo $this->renderAjax('error_lecteur',['code'=>$code]);
       }else{
           echo $this->renderAjax('empr',['code'=>$code]);
       }
    }
       
    }
    
    public function actionGetRaportParam($date1, $date2){
        echo $this->renderAjax('raport_param',['date1'=>$date1,'date2'=>$date2]);
    }
    
    
    public function actionGetRapportVisite(){
       
      echo  $this->renderAjax('raport');  
    }
    
    public function actionGetHeure(){
        echo date('Y-m-d h:i:s');
    }
    
    /**
     * 
     * 
     */
    public function actionGetTotalVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $visite_par_jour = LogipamPresence::find()->where(['=','date_presence',$today])->count();  
        echo $visite_par_jour; 
    }
    
     public function actionGetTotalAdherantByLocation($location){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $visite_par_jour = LogipamPresence::find()->where(['=','date_presence',$today])->andWhere(['=','location',$location])->count();  
        echo $visite_par_jour; 
    }
    
    public function actionGetTotalFemaleVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = 2 AND lp.date_presence = '$today'";
        $visite_female_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_female_jour; 
    }
    
    public function actionGetTotalAdherantByGenderLocation($location,$sexe){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = $sexe AND lp.location = $location AND lp.date_presence = '$today'";
        $visite_female_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_female_jour; 
    }
    
    public function actionGetTotalMaleVisite(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT lp.id FROM logipam_presence lp INNER JOIN empr e ON (lp.id_empr = e.id_empr) WHERE  e.empr_sexe = 1 AND lp.date_presence = '$today'";
        $visite_male_jour = LogipamPresence::findBySql($sql_str)->count(); 
        echo $visite_male_jour; 
    }
    
    public function actionGetAdhesionDepassee(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id_empr, empr_date_expiration FROM empr WHERE empr_date_expiration <= '$today'"; 
        $total_adhesion_depassee = LogipamPresence::findBySql($sql_str)->count(); 
        echo $total_adhesion_depassee; 
    }
    
    public function actionGetTotalVisitor(){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id FROM logipam_visiteur WHERE date_presence = '$today'"; 
        $total_visitor = \app\models\LogipamVisiteur::findBySql($sql_str)->count(); 
        echo $total_visitor; 
    }
    
    public function actionGetTotalVisitorByLocation($location){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id FROM logipam_visiteur WHERE date_presence = '$today' AND location = $location"; 
        $total_visitor = \app\models\LogipamVisiteur::findBySql($sql_str)->count(); 
        echo $total_visitor; 
    }
    
    public function actionGetTotalVisitorSexe($sexe){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id FROM logipam_visiteur WHERE date_presence = '$today' AND gender = $sexe"; 
        $total_visitor = \app\models\LogipamVisiteur::findBySql($sql_str)->count(); 
        echo $total_visitor; 
    }
    
  public function actionGetTotalVisitorBySexeLocation($location,$sexe){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $sql_str = "SELECT id FROM logipam_visiteur WHERE date_presence = '$today' AND gender = $sexe AND location = $location"; 
        $total_visitor = \app\models\LogipamVisiteur::findBySql($sql_str)->count(); 
        echo $total_visitor; 
  }

public function actionGetDailyInscritByLocation($location){
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d');
    $total_inscrit = Empr::find()->where(['=','DATE(empr_creation)',$today])->andWhere(['=','empr_location',$location])->count();  
    echo $total_inscrit; 
}

public function actionGetDailyInscritGenderLocation($location, $sexe){
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d');
    $total_inscrit = Empr::find()->where(['=','DATE(empr_creation)',$today])->andWhere(['=','empr_location',$location])->andWhere(['=','empr_sexe',$sexe])->count();  
    echo $total_inscrit; 
}

public function actionGetPretByLocation($location){
    $sql_str = "SELECT *  FROM pret p INNER JOIN empr e ON (e.id_empr = p.pret_idempr) WHERE e.empr_location = $location"; 
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d');
    $data_pret = \app\models\Pret::findBySql($sql_str)->all();
    $nombre_retard = 0; 
    $pret_encours = 0;
    foreach($data_pret as $dp){
        if($dp->pret_retour < $today){
            $nombre_retard++; 
        }else{
            $pret_encours++;
        }
        
    }
    $data = [];
    $data[0] = $pret_encours;
    $data[1] = $nombre_retard;
    $data_json = json_encode($data,JSON_NUMERIC_CHECK);
    return $data_json; 
    
}

public function actionGetTotalVisiteDaily($location){
    $sql_str_m = "select day_presence, count(day_presence) total_jour  from logipam_presence  lp INNER JOIN empr e ON (e.id_empr = lp.id_empr ) where yearweek(date_presence) = yearweek(curdate()) AND location = $location and e.empr_sexe = 1 Group BY day_presence order by day_presence";
    $data_presence_m = LogipamPresence::findBySql($sql_str_m)->all(); 
    $sql_str_f = "select day_presence, count(day_presence) total_jour  from logipam_presence  lp INNER JOIN empr e ON (e.id_empr = lp.id_empr ) where yearweek(date_presence) = yearweek(curdate()) AND location = $location and e.empr_sexe = 2 Group BY day_presence order by day_presence";
    $data_presence_f = LogipamPresence::findBySql($sql_str_f)->all(); 
    
    $tablo_tot['male'] = [0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0];
    $tablo_tot['female'] = [0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0];
   
    foreach($data_presence_m as $dp){
       $tablo_tot['male'][$dp['day_presence']] = $dp['total_jour'];
    }
    
    foreach($data_presence_f as $dp){
       $tablo_tot['female'][$dp['day_presence']] = $dp['total_jour'];
       
    }
    
    $data_json = json_encode($tablo_tot,JSON_NUMERIC_CHECK);
    return $data_json; 
}

public function actionGetTotalVisiteMonth($location){
    $sql_str_f = "SELECT COUNT(*) as total_mois, MONTH(date_presence) as mois FROM logipam_presence lp  INNER JOIN empr e ON (e.id_empr = lp.id_empr) WHERE YEAR(date_presence) = YEAR(NOW()) AND lp.location = $location AND e.empr_sexe = 2 GROUP BY MONTH(date_presence)";
    $data_presence_f = LogipamPresence::findBySql($sql_str_f)->all();
    $sql_str_m = "SELECT COUNT(*) as total_mois, MONTH(date_presence) as mois FROM logipam_presence lp  INNER JOIN empr e ON (e.id_empr = lp.id_empr) WHERE YEAR(date_presence) = YEAR(NOW()) AND lp.location = $location AND e.empr_sexe = 1 GROUP BY MONTH(date_presence)";
    $data_presence_m = LogipamPresence::findBySql($sql_str_m)->all();
    $tablo_tot['female'] = [0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0];
    $tablo_tot['male'] = [0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0];
    foreach($data_presence_f as $dp){
        $tablo_tot['female'][$dp['mois']-1] = $dp['total_mois'];
        
    }
    
    foreach($data_presence_m as $dp){
        $tablo_tot['male'][$dp['mois']-1] = $dp['total_mois'];
        
    }
    
    $data_json = json_encode($tablo_tot,JSON_NUMERIC_CHECK);
    return $data_json;  
    
}

public function actionGetVisiteByCategorie($location){
    $str_sql = "SELECT COUNT(*) as total_cat, ec.libelle as label_cat FROM logipam_presence lp INNER JOIN empr e ON (e.id_empr = lp.id_empr) INNER JOIN empr_categ ec ON (ec.id_categ_empr = e.empr_categ) WHERE lp.location = $location AND MONTH(lp.date_presence) = MONTH(NOW()) AND YEAR(lp.date_presence) = YEAR(NOW()) GROUP by ec.libelle";
    $data_presence = LogipamPresence::findBySql($str_sql)->all();
    $tablo_tot = []; 
    $k =0;
    foreach($data_presence as $dp){
        $tablo_tot['label'][$k] = $dp['label_cat'];
        $tablo_tot['total'][$k] = $dp['total_cat']; 
        $k++;
    }
    $data_json = json_encode($tablo_tot,JSON_NUMERIC_CHECK);
    return $data_json;  
}
    
    
    public function actionGetRapportListe(){
        echo $this->renderAjax('liste');
    }
    
    
  public function actionUpload(){
        $fileName = 'file';
        $uploadPath = '../../../../photos/lecteurs';
       
        if (isset($_FILES[$fileName])) {
            
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            
           // echo $empr_code; 
            /*
            if(isset($data_empr)){
                $empr_cb = $empr_code; 
                $id_empr = $data_empr->id_empr;
                $prenom = $data_empr->empr_prenom;
                $nom = $data_empr->empr_nom; 
                $sexe = $data_empr->empr_sexe; 
                $date_adhesion = $data_empr->empr_date_adhesion;
                $date_expiration = $data_empr->empr_date_expiration;
              */  
                
            
                if ($file->saveAs($uploadPath . '/' . $file->name)) {
                   
                   $file_path = $uploadPath . '/' . $file->name;
                    // jpg  change the dimension 750, 450 to your desired values
                   $img = $this->resize_imagejpg($file_path, 200, 200);
                    // again for jpg
                    imagejpeg($img, $uploadPath . '/' . $file->name);
                   // $img = resize_imagejpg($file_path, 200, 200);
                    $carteMembre = new LogipamCarteMembre();
                   // $data_empr = new Empr();
                    $empr_code = substr($file->name,0,-4);
                    
                   $data_empr = Empr::findBySql("SELECT empr_prenom, empr_nom, empr_sexe, empr_date_adhesion, empr_date_expiration, empr_location FROM empr WHERE empr_cb = '$empr_code'")->asArray()->all();
                    //print_r($data_empr);
                   // $empr_cb = $empr_code; 
                    //$id_empr = $data_empr->id_empr;
                           
                    $prenom = $data_empr[0]['empr_prenom'];
                    $nom = $data_empr[0]['empr_nom']; 
                    $sexe = $data_empr[0]['empr_sexe']; 
                    $date_adhesion = $data_empr[0]['empr_date_adhesion'];
                    $date_expiration = $data_empr[0]['empr_date_expiration'];
                    $location = $data_empr[0]['empr_location'];
                    
                    
                //Now save file data to database
                  
                    $carteMembre->empr_cb = $empr_code; 
                    $carteMembre->image_name = $file->name;//$dataempr->getPrenom($empr_code);
                    $carteMembre->date_ajout = date('Y-m-d h:m:s');
                    $carteMembre->prenom = $prenom; 
                    $carteMembre->nom = $nom; 
                    $carteMembre->sexe = $sexe; 
                    $carteMembre->date_adhesion = $date_adhesion; 
                    $carteMembre->date_expiration = $date_expiration;
                    $carteMembre->location = $location;
                    $carteMembre->save(); 
                     
                /*
                    $model = new Migration(); 
                    $model->file_name = $file->name;
                    $model->is_migrate = 0; 
                    $model->is_delete = 0; 
                    $model->date_upload = date('Y-m-d h:m:s');
                    $model->migrate_by = Yii::$app->user->identity->username;
                    $model->save();
                 * 
                 */
                  
                echo \yii\helpers\Json::encode($file);
            }
                
            /*    
                
            }else{
                $id_empr = null;
            }
             * 
             */
            //Print file data
           // print_r($file);

            
    }else{
        
        return $this->renderAjax('upload'); 
        
        
    }

    return false;
    }
    
    public function actionGetLisLekte(){
        echo $this->renderAjax('listlekte'); 
    }
    
    public function actionGetLisLekteSearch($value){
        echo $this->renderAjax('listlektesearch',['value'=>$value]); 
    }
    
    public function actionGetLisEnprime($value){
        $current_user = $this->getUsername(); 
        echo $this->renderAjax('lis-kat-enprime',['value'=>$value,'current_user'=>$current_user]); 
    }
    
    public function actionGetNewLisEnprime($value){
        $current_user = $this->getUsername();
        echo $this->renderAjax('new-lis-kat-enprime',['value'=>$value,'current_user'=>$current_user]); 
    }
    
    public function actionCartepdf($listid){
        //$this->layout = "pdf_layout";
        $content = $this->renderPartial('cartepdf',['listid'=>$listid]);
        $pdf = new Pdf();
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s');
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
				//'SetHeader'=>['Donnee Enquetes Avril 2015'],
				'SetFooter'=>['{PAGENO}'],
			];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
        $array_id = explode(",",$listid); 
        for($i=0;$i<sizeof($array_id);$i++){
            $carte = LogipamCarteMembre::findOne($array_id[$i]);
            $carte->is_print = 1;
            $carte->date_print = date("y-m-d h:i:s");
            $carte->save();
        }
    	return $pdf->render();

        
    }
    
    public function actionNewcartepdf($listid){
        //$this->layout = "pdf_layout";
        $content = $this->renderPartial('newcartepdf',['listid'=>$listid]);
        //$en_tete =  $this->renderPartial('en-tete-carte');
        $pdf = new Pdf(
                [
                    'mode' => 'utf-8',
                    'format'=>[57,88],
                    'orientation'=>'P'
                ]
                );
       
        
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s').".pdf";
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
        //$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->cssFile = '@vendor/css/style_carte.css';
        $pdf->cssInline =  '.foto-id{border-radius: 10px;}';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(57,88);
    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;
    	$pdf->destination = Pdf::DEST_BROWSER;
        $pdf->marginTop = 0;
        $pdf->marginTop = 0;
        $pdf->marginLeft = 0;
        $pdf->marginRight = 0;
    	
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
				//'SetHeader'=>['Donnee Enquetes Avril 2015'],
				//'SetFooter'=>['{PAGENO}'],
			];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	//'tabSpaces' => 4
    	];
        //$pdf = $pdf->api;
        //$pdf->setHTMLHeader('@vendor/css/style_carte.css',\Mpdf\HTMLParserMode::HEADER_CSS);
       // $pdf->SetHTMLHeader($en_tete);
    	
    	// return the pdf output as per the destination setting
        $array_id = explode(",",$listid); 
        for($i=0;$i<sizeof($array_id);$i++){
            $carte = LogipamCarteMembre::findOne($array_id[$i]);
            $carte->is_print = 1;
            $carte->date_print = date("y-m-d h:i:s");
            $carte->save();
        }
    	return $pdf->render();
        //return $pdf->Output("carte-lecteur-biblio".date('Y-m-d h:i:s'),'I');

        
    }
    
    
    public function actionPrintonecard($id){
        $this->layout = "pdf_layout";
        $content = $this->renderPartial('printonecard',['id'=>$id]);
        $pdf = new Pdf();
        $pdf->filename = "carte-lecteur-biblio".date('Y-m-d h:i:s');
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = $pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Carte membre biblio'];
    	$pdf->methods = [
	    	'SetHeader'=>[''],
	    	'SetFooter'=>[''],
    	];
    	
    	$pdf->options = [
	    	'title' => 'Carte membre biblio',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
    	return $pdf->render();

        
    }
    
    public function actionCartehtml($listid){
        $this->layout = "pdf_layout";
        return $this->render('cartehtml',['listid'=>$listid]);
    }
    
    public function actionDeletecarte($id){
        $carte = LogipamCarteMembre::findOne($id);
        $file_name = $carte->image_name;
        $file_to_delete = '../../../../photos/lecteurs/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'.$file_to_delete.'"');
        $carte->delete(); 
        
    }
    
    public function actionDeletekat($id){
        $sql = "SELECT * FROM logipam_carte_membre WHERE empr_cb = '$id'"; 
        $carte = LogipamCarteMembre::findBySql($sql)->all();
        $file_name = $carte->image_name;
        $id_kat = $carte->id;
        $carte1 = LogipamCarteMembre::findOne($id_kat);
        $file_to_delete = '../../../../photos/lecteurs/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'.$file_to_delete.'"');
        $carte1->delete(); 
        
    }
    
    // for jpg 
    /**
     * 
     * @param type $file
     * @param type $w
     * @param type $h
     * @return type
     */
public function resize_imagejpg($file, $w, $h) {
   list($width, $height) = getimagesize($file);
   $src = imagecreatefromjpeg($file);
   $dst = imagecreatetruecolor($w, $h);
   imagecopyresampled($dst, $src, 0, 0, 0, 0, $w, $h, $width, $height);
   return $dst;
}


public function actionCorrectDuplicateAuthor(){
    $authors_double = \app\models\Authors::findBySql("select author_id, group_concat(author_id separator ',') as 'all_dup_id' from authors group by index_author having count(author_id) > 1 order by count(author_id) DESC, index_author")->asArray()->all();
    $k = 1;
    $is_update_all = FALSE; 
    foreach($authors_double as $ad){
       
        $array_id_dup = explode(",",$ad['all_dup_id']);
        for($i=0; $i<sizeof($array_id_dup); $i++){
            if($ad['author_id']!=$array_id_dup[$i]){
                $responsabilitys = \app\models\Responsability::find()->where(['responsability_author'=>$array_id_dup[$i]])->all();
                
                foreach($responsabilitys as $responsability){
                    $responsability->responsability_author = $ad['author_id'];
                    $responsability->responsability_fonction = '070';
                    if($responsability->save()){
                       $is_update_all = TRUE;
                       
                    } 
                }
                //echo $ad['author_id'].' -> '.$array_id_dup[$i].'<br/>';
               
           }
               
        }
        $k++;
    }
    
    if($is_update_all){
        echo "Tout bagay byen pase $k fwa";
    }else{
        echo "Bagay yo mal pase a mort";
    }
  
}


public function actionCorrectDuplicatePublisher(){
    $publishers_double = \app\models\Publishers::findBySql("select index_publisher as Editeur, ed_id, group_concat(ed_id separator ',') as 'dup_ed_id', count(ed_id) as Nombre from publishers group by Editeur having Nombre> 1
    UNION select ed_name as Editeur, ed_id, GROUP_CONCAT(ed_id SEPARATOR ',') as 'dup_ed_id', count(ed_id) as Nombre from publishers group by Editeur having Nombre> 1 order by Nombre DESC, Editeur")->asArray()->all();
    $k = 1;
    $is_update_all = FALSE; 
    foreach($publishers_double as $pd){
        $array_id_dup = explode(",",$pd['dup_ed_id']);
        for($i=0; $i<sizeof($array_id_dup); $i++){
            if($pd['ed_id']!=$array_id_dup[$i]){
                $notices = \app\models\Notices::find()->where(['ed1_id'=>$array_id_dup[$i]])->all();
                
                foreach($notices as $notice){
                    $notice->ed1_id = $pd['ed_id'];
                    if($notice->save()){
                       $is_update_all = TRUE;
                       
                    } 
                }
                
                $notices2 = \app\models\Notices::find()->where(['ed2_id'=>$array_id_dup[$i]])->all();
                foreach($notices2 as $notice2){
                    $notice2->ed2_id = $pd['ed_id'];
                    if($notice2->save()){
                       $is_update_all = TRUE;
                       
                    } 
                }
                 
                //echo $k.'-'.$pd['ed_id'].' -> '.$array_id_dup[$i].'<br/>';
               
           }
    }
    $k++;
        }
        if($is_update_all){
            echo "Tout bagay byen pase $k fwa";
        }else{
            echo "Bagay yo mal pase a mort";
        }
    }
    
public function actionCorrectDuplicateCollection(){
    $collections_double = \app\models\Collections::findBySql("select index_coll as Collection, collection_id, group_concat(collection_id separator ',') as 'col_dup_id', count(collection_id) as Nombre from collections, publishers WHERE collection_parent=ed_id group by Collection having Nombre > 1 order by Nombre DESC, Collection")->asArray()->all();
    $k = 1;
    $is_update_all = FALSE; 
    foreach($collections_double as $cd){
        $array_id_dup = explode(",",$cd['col_dup_id']);
        for($i=0; $i<sizeof($array_id_dup); $i++){
            if($cd['collection_id']!=$array_id_dup[$i]){
                $notices = \app\models\Notices::find()->where(['coll_id'=>$array_id_dup[$i]])->all();
                
                foreach($notices as $notice){
                    $notice->coll_id = $cd['collection_id'];
                    if($notice->save()){
                       $is_update_all = TRUE;
                       
                    } 
                }
                //echo $k.'-'.$pd['ed_id'].' -> '.$array_id_dup[$i].'<br/>';
               
           }
    }
    $k++;
        }
        if($is_update_all){
            echo "Tout bagay byen pase $k fwa";
        }else{
            echo "Bagay yo mal pase a mort";
        }
    }

public function getUsername(){
        if(isset($_SERVER['HTTP_COOKIE'])){
            $data = $_SERVER['HTTP_COOKIE']; 
            $array_sess = explode(";",$data);
            // Recherche le login dans le cookie du server 
            $m_array = preg_grep('/PhpMyBibli-LOGIN/i', $array_sess);
            if(!empty($m_array)){
            $str_array = array_values($m_array)[0];
            $user_pmb_clean = str_replace('PhpMyBibli-LOGIN=',"",$str_array);
            return preg_replace('/\s+/', '', $user_pmb_clean);
                $str_array = "";
                return $str_array; 
            }
            
           
        }else{
            
             return null; 
        }
    }
    
    
 public function actionTestUser(){
     return $this->getUserName(); 
 }   

public function actionGetDashboard(){
    $current_user = $this->getUsername();
    echo $this->renderAjax('dashboard',['current_user'=>$current_user]);
    
}    
    
}