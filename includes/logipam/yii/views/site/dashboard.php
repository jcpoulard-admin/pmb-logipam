<?php
// Recherche la location de l'utilisateur connecte 
use app\models\Users; 
use app\models\DocsLocation; 
//Recherche le nom de la location
$user_data = Users::findOne(['username'=>$current_user]);  //
$default_location = null; 
if(!empty($user_data)){
    $default_location = $user_data['deflt_docs_location']; 
}else{
    $default_location = null; 
}

if($default_location !== null){
    $location = DocsLocation::findOne(['idlocation'=>$default_location]);
    $location_name = $location['name'];
}else{
    $location_name = "";
}
// print_r($user_data);

// Copnstruction de la list de location 
$all_location_data = DocsLocation::find()->all(); 

?>
<div id="gwo-dachbod">
    
<div class="row chwa-location">
    <input id="hide-location" value="<?= $default_location; ?>" type="hidden">
    <div class="col-lg-6">
        <h3><span id="location-name"><?= $location_name; ?></span></h3>
    </div>
    <div class="col-lg-6 dwat">
        
        <select id="select-location">
            <option >Choisir une localisation</option>
            <?php
                foreach($all_location_data as $ald){
                    ?>
            <option value="<?= $ald['idlocation'] ?>"><?= $ald['name']; ?></option>
            <?php 
                }
            ?>
        </select>
    </div>
</div>



<div class="row zon-dachbod" style="padding: 2px;">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
                <h3><span id="total-vizit"></span></h3>

                <p>Total visite adh&eacute;rent</p>
                <i class="fa fa-male"></i> : <span id="m-adherant"></span> <span id="v-male"></span> <i class="fa fa-female"></i> : <span id="f-adherant"></span> <span id="v-female"></span>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
         
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
                <h3><span id='total_visitor'></span></h3>

              <p>Total visiteurs</p>
              <i class="fa fa-male"></i> : <span id="v_male"></span> <i class="fa fa-female"></i> : <span id="v_female"></span>
            </div>
            <div class="icon">
              <i class="fa fa-venus"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
                <h3><span id='total-inscrit'></span></h3>

              <p>Total nouveaux inscrits du jour</p>
              <i class="fa fa-male"></i> : <span id="total-inscrit-m"></span> <i class="fa fa-female"></i> : <span id="total-inscrit-f"></span>
            </div>
            <div class="icon">
              <i class="fa fa-mars"></i>
            </div>
            
          </div>
        </div>
       
      </div>

<div class="input-group input-group-sm zon-sezi">
    <input class="form-control" id="kod-lekte" type="text" placeholder="Saisir le code du lecteur">
    <span class="input-group-btn">
        <button type="button" id="btnSave" class="btn btn-info btn-flat"><i class="fa fa-save"></i> Enregistrer</button>
    </span>
</div>

<div id="espas-lekte">
    
</div>

<div class="row zon-graph">
    <div class="col-lg-8">
        <canvas id="bar-chart" height="125"></canvas>
    </div>
    
    <div class="col-lg-4">
        <canvas id="myChart" width="200" height="125"></canvas>
    </div>
</div>

<div class="row zon-graph">
    <div class="col-lg-8">
        <canvas id="bar-chart2" height="125"></canvas>
    </div>
    
    <div class="col-lg-4">
        <canvas id="myChart2" width="200" height="125"></canvas>
    </div>
</div>

</div>

<div id='upload-foto'>
    
</div>

<div id="zon-kat-enprime">
    <p><label>Recherche carte d&eacute;j&agrave; imprim&eacute;e</label></p>
    <input type="text" id="idSearch" placeholder="Rechercher" />
    <div id="lis-enprime">
        
    </div>
</div>

<script>
$(document).ready(function(){
  
  var location_dur = $('#hide-location').val();
  var data_pret; 
  
  // alert(location_dur);
  
  totalAdherantByLocation();
  totalAherantByGenderLocationM();
  totalAherantByGenderLocationF();
  totalVisitorByLocation();
  totalVisitorSexeF();
  totalVisitorSexeM();
  totalDailyInscritByLocation();
  totalDailyInscritGenderLocationF();
  totalDailyInscritGenderLocationM();
  dataPret();
  dataPresenceDaily();
  dataPresenceMonthly();
  dataPresenceCat();
   $("#zon-kat-enprime").hide();
  
  $("#addPhoto").click(function(){
            $("#gwo-dachbod").hide();
            $('#upload-foto').show();
            $("#zon-kat-enprime").hide();
            $("#espas-dashboard").hide();
            $.get('includes/logipam/yii/web/index.php?r=site/upload',{},function(data){
                $('#upload-foto').html(data);
            });
        });
        
  $("#dashboard").click(function(){
            $("#lis-lekte").hide();
            $("#an-tete").hide();
            $('#upload-foto').hide();
            $("#zon-kat-enprime").hide();
            $("#gwo-dachbod").show();
            
            $.get('includes/logipam/yii/web/index.php?r=site/get-dashboard',{},function(data){
               $("#espas-dashboard").show(); 
               $("#espas-dashboard").html(data);
               
            });
     }); 
     
     $("#takePrezans").click(function(){
            $('#upload-foto').hide();
            $("#an-tete").show();
            $("#lekte").empty();
            $("#empr_cb").val("");
            $("#empr_cb").focus(); 
            $(".zon-dachbod").show();
            $(".zon-sezi").show();
            $("#zon-kat-enprime").hide();
           // $("#espas-dashboard").hide();
            $("#gwo-dachbod").show();
            // Afficher le rapport en dessous 
            var valeur = 1; 
            $.get('includes/logipam/yii/web/index.php?r=site/get-rapport-visite',{valeur : valeur},function(data){
            
                $('#lekte').html(data);
            });
        });
        
      $("#newCard").click(function(){
            
            $("#lis-lekte").hide();
            $("#an-tete").hide();
            $('#upload-foto').hide();
            $("#zon-kat-enprime").show();
            $("#espas-dashboard").hide();
             $("#gwo-dachbod").hide();
          // var valeur = $("#idSearch").val();
          var valeur = null;
            $.get('includes/logipam/yii/web/index.php?r=site/get-new-lis-enprime',{value: valeur},function(data){
                $('#lis-enprime').html(data);
            });
        }); 
        
        $("#idSearch").keyup(function(){
            
            var valeur = $("#idSearch").val();
            $.get('includes/logipam/yii/web/index.php?r=site/get-lis-enprime',{value : valeur},function(data){
            
                    $('#lis-enprime').html(data);
            //setTimeout(tablo, 10000);
            });
            
        });
  
  
  function totalAdherantByLocation(){
      
      $.get('includes/logipam/yii/web/index.php?r=site/get-total-adherant-by-location',{location:location_dur},function(data){
            $('#total-vizit').html(data);
            setTimeout(totalAdherantByLocation, 10000);
            });
  }
  
  function totalAherantByGenderLocationM(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-total-adherant-by-gender-location',{location:location_dur,sexe:1},function(data){
            $('#m-adherant').html(data);
            setTimeout(totalAherantByGenderLocationM, 10000);
            });
  }
  
  function totalAherantByGenderLocationF(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-total-adherant-by-gender-location',{location:location_dur,sexe:2},function(data){
            $('#f-adherant').html(data);
            setTimeout(totalAherantByGenderLocationF, 10000);
            });
  }
  
  // GetTotalVisitorByLocation
  function totalVisitorByLocation(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-total-visitor-by-location',{location:location_dur},function(data){
            $('#total_visitor').html(data);
            setTimeout(totalVisitorByLocation, 10000);
            });
  }
  //GetTotalVisitorBySexeLocation
  function totalVisitorSexeF(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-total-visitor-by-sexe-location',{location:location_dur,sexe:2},function(data){
            $('#v_female').html(data);
            setTimeout(totalVisitorSexeF, 10000);
            });
  }
  
  function totalVisitorSexeM(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-total-visitor-by-sexe-location',{location:location_dur,sexe:1},function(data){
            $('#v_male').html(data);
            setTimeout(totalVisitorSexeM, 10000);
            });
  }
  
  // GetDailyInscritByLocation
  function totalDailyInscritByLocation(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-daily-inscrit-by-location',{location:location_dur},function(data){
            $('#total-inscrit').html(data);
            setTimeout(totalDailyInscritByLocation, 10000);
            });
  }
  
  // GetDailyInscritGenderLocation
  
  function totalDailyInscritGenderLocationF(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-daily-inscrit-gender-location',{location:location_dur,sexe:2},function(data){
            $('#total-inscrit-f').html(data);
            setTimeout(totalDailyInscritGenderLocationF, 10000);
            });
  }
  
  function totalDailyInscritGenderLocationM(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-daily-inscrit-gender-location',{location:location_dur,sexe:1},function(data){
            $('#total-inscrit-m').html(data);
            setTimeout(totalDailyInscritGenderLocationM, 10000);
            });
  }
  
  function dataPret(){
        $.get('includes/logipam/yii/web/index.php?r=site/get-pret-by-location',{location:location_dur},function(data){
        data_pret = JSON.parse(data);
                
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['A jour', 'En retard'],
                datasets: [{
                    label: 'Total prets',
                    data: data_pret,
                    backgroundColor: [
                        
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: [
                        
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 99, 132, 1)',

                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        display: false,    
                        ticks: {
                            beginAtZero: true
                        }
                    }], 
                    xAxes: [{
                            display: false
                    }]
                },
                title: {
                    display: true,
                    text: 'TOTAL PRET'
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        //fontColor: 'rgb(255, 99, 132)'
                    }
                }
            }
        });
     });
  }
  
  function dataPresenceDaily(){
      
        $.get('includes/logipam/yii/web/index.php?r=site/get-total-visite-daily',{location:location_dur},function(data){
                
                var data_raw = JSON.parse(data);
                var data_jour_m = data_raw.male;
                var data_jour_f = data_raw.female;
                
                // Place le graphe ici et passer les variables apres 
var ctx1 = document.getElementById('bar-chart');
var myChart1 = new Chart(ctx1, {
    type: 'bar',
    data: {
        labels: ['Dimanche','Lundi','Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'], //data_jour_m, 
        datasets: [
            {
            label: 'Homme',
            data: data_jour_m,
            
            backgroundColor: [
               'rgba(83, 157, 200, 0.5)',
               'rgba(83, 157, 200, 0.5)',
               'rgba(83, 157, 200, 0.5)',
               'rgba(83, 157, 200, 0.5)',
               'rgba(83, 157, 200, 0.5)',
               'rgba(83, 157, 200, 0.5)',
               'rgba(83, 157, 200, 0.5)',
               
                
                    
            ],
            borderColor: [
                
                
            ],
            borderWidth: 1
        },
        {
            label: 'Femmes',
            data: data_jour_f,
            backgroundColor: [
                'rgba(247, 163, 92, 0.5)',
                'rgba(247, 163, 92, 0.5)',
                'rgba(247, 163, 92, 0.5)',
                'rgba(247, 163, 92, 0.5)',
                'rgba(247, 163, 92, 0.5)',
                'rgba(247, 163, 92, 0.5)',
                'rgba(247, 163, 92, 0.5)',
                
            ],
            borderColor: [
                
                
            ],
        }
    ]
    },
    options: {
        scales: {
            yAxes: [{
                display: false,    
                gridLines: {
                    drawBorder: false,
                },
                
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                    display: true,
            }]
        },
        title: {
            display: true,
            text: 'Nombre de presences par jour de la semaine ( Hommes/Femmes)',
            fontSize: 16,
        },
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                //fontColor: 'rgb(255, 99, 132)'
            }
        }
    }
});
                
         });
         
         
 

  }
  
  function dataPresenceMonthly(){
       $.get('includes/logipam/yii/web/index.php?r=site/get-total-visite-month',{location:location_dur},function(data){
           var data_raw = JSON.parse(data);
           var data_month_m= data_raw.male;
           var data_month_f = data_raw.female;
           
           // Place les graphes ici 
           var ctx2 = document.getElementById('bar-chart2');
var myChart2 = new Chart(ctx2, {
    type: 'bar',
    data: {
        labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
        datasets: [
            {
            label: 'Hommes',
            data: data_month_m,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                
            ],
            borderColor: [
                
            ],
            borderWidth: 1
        },
        {
            label: 'Femmes',
            data: data_month_f,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
             ],
            borderColor: [
               
            ],
            borderWidth: 1
        },
        
    ]
    },
    options: {
        scales: {
            yAxes: [{
                display: false,
                gridLines: {
                    drawBorder: false,
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        title: {
            display: true,
            text: 'Nombre de presences par mois ( Hommes/Femmes)',
            fontSize: 16,
        },
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                //fontColor: 'rgb(255, 99, 132)'
            }
        }
    }
});
           
       });
  }
  
  function dataPresenceCat(){
      $.get('includes/logipam/yii/web/index.php?r=site/get-visite-by-categorie',{location:location_dur},function(data){
           var data_raw = JSON.parse(data);
           var data_label = data_raw.label; 
           var data_total = data_raw.total;
           
           // Ajoute graphe ici 
          var ctx3 = document.getElementById('myChart2');
          var myChart3 = new Chart(ctx3, {
          type: 'pie',
    data: {
        labels: data_label, //['A valider', 'Confirmees'],
        datasets: [{
           // label: 'Reservations',
            data: data_total, //[12, 39, ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                        display: false,    
                        ticks: {
                            beginAtZero: true
                        }
                    }], 
                    xAxes: [{
                            display: false
                    }]
        },
        title: {
            display: true,
            text: 'Nombre de presences par categories',
            fontSize: 16
        },
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                //fontColor: 'rgb(255, 99, 132)'
            }
        }
    }
});


           
       });
  }
  
  
  // GetTotalAdherantByGenderLocation
    
 // Enregistre les membres et les visiteurs 
 $("#btnSave").click(function(){
            var valeur = $("#kod-lekte").val();
            alert(valeur);
            $.get('includes/logipam/yii/web/index.php?r=site/get-lecteur-info',{code : valeur},function(data){
            $('#espas-lekte').html(data);
            
            });
            /*
            totalVisite(); 
            totalFemaleVisite();
            totalMaleVisite();
            totalVisiteur(); 
            totalVisiteurM();
            totalVisiteurF();
            */
            $("#kod-lekte").val("");
            $("#kod-lekte").focus();
        });
        
    $(document).keypress(function(e) {
        if(e.which === 13) {
            var valeur = $("#kod-lekte").val();
            $.get('includes/logipam/yii/web/index.php?r=site/get-lecteur-info',{code : valeur},function(data){
            $('#espas-lekte').html(data);
            
            });
            /*
            totalVisite(); 
            totalFemaleVisite();
            totalMaleVisite();
            totalVisiteur(); 
            totalVisiteurM();
            totalVisiteurF();
             * 
             */
            $("#kod-lekte").val("");
            $("#kod-lekte").focus();
        }
    });
 
 $('#select-location').change(function(){
     var location_id = $('#select-location option:selected').val(); 
     var location_name = $('#select-location option:selected').text();
     $('#location-name').html(location_name);
     location_dur = location_id;
     
     $("#myChart").html("");
     $("#myChart2").html("");
     $("#bar-chart").html("");
     $("#bar-chart2").html("");
     
     totalAdherantByLocation();
     totalAherantByGenderLocationM();
     totalAherantByGenderLocationF();
     totalVisitorByLocation();
     totalVisitorSexeF();
     totalVisitorSexeM();
     totalDailyInscritByLocation();
     totalDailyInscritGenderLocationF();
     totalDailyInscritGenderLocationM();
     dataPret();
     dataPresenceDaily();
     dataPresenceMonthly();
     dataPresenceCat();
     //alert(location_id);
 });
    
    
// Pour la navigation 









    });
</script>