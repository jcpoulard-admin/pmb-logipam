<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use app\models\LogipamCarteMembre; 
use app\models\DocsLocation; 

$sql_str = "SELECT * FROM logipam_carte_membre WHERE is_print is NULL";
$data_carte = LogipamCarteMembre::findBySql($sql_str)->AsArray()->all(); 

?>

<div class="alert alert-info alert-dismissible" id="show-delete">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> Supression!</h4>
                La carte de lecteur a &eacute;t&eacute; suprim&eacute; avec succ&egrave;s !
</div>

<button id='print-carte' class='btn btn-success'>Imprimer carte en groupe</button>
<input type="text" placeholder="Rechercher" id="idSearch"/>
<div id="liste-recherche"></div>
<div id="liste-brut">
<table id="tablo-kat" class="display" cellspacing="0" width="100%">
    <thead class="table table-bordered table-hover">
        <tr>
            <th>
                <input type='checkbox' id='check-all'>
            </th>
            <th>Code</th>
            <th>Pr&eacute;mom</th>
            <th>Nom</th>
            <th>Sexe</th>
            <th>Localisation</th>
            <th>Date adh&eacute;sion</th>
            <th>Date expiration</th>
            <th>&nbsp;&nbsp;&nbsp;</th>
            <th>&nbsp;&nbsp;&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        
            for($i=0; $i<sizeof($data_carte);$i++){
                $sexe = null;
                if($data_carte[$i]['sexe']==1){
                    $sexe = "Masculin";
                }else{
                    $sexe = "F&eacute;minin";
                }
                ?>
    
        <tr data-lecteur="<?= $data_carte[$i]['id']?>"> 
            <td>
                <input type='checkbox' class='choix-lekte' value='<?= $data_carte[$i]['id']?>'>
            </td>
                
            <td>
                <a href="includes/logipam/yii/web/index.php?r=site/cartepdf&listid=<?= $data_carte[$i]['id'] ?>" target="_blank" data-toggle="tooltip" data-html="true" title=''>            
            <?= $data_carte[$i]['empr_cb']?>
            </a>
            </td>
            
            <td><?= $data_carte[$i]['prenom']?></td>
            <td><?= $data_carte[$i]['nom']?></td>
            <td><?= $sexe ?></td>
            <td>
                <?php 
                if(DocsLocation::findOne($data_carte[$i]['location']) !== NULL){
                    echo DocsLocation::findOne($data_carte[$i]['location'])->name; 
                }else{
                    echo "";
                }
                ?>
            </td>
            <td><?= Yii::$app->formatter->asDate($data_carte[$i]['date_adhesion']) ?></td>
            <td><?= Yii::$app->formatter->asDate($data_carte[$i]['date_expiration']) ?></td>
            <td>
                <a href="includes/logipam/yii/web/index.php?r=site/cartepdf&listid=<?= $data_carte[$i]['id'] ?>" target="_blank" data-toggle="tooltip" data-html="true" title='Imprimer une carte'>
                    <i class="fa fa-print"></i>
                </a>
            </td>
            <td>
                <a href="#" class="delete-carte" data-idcarte="<?= $data_carte[$i]['id'] ?>">
                    <i class="fa fa-trash"></i>
                </a>
            </td>
        </tr>
        <?php 
            }
        ?>
    </tbody>
</table>
</div>

<script type="text/javascript">
    var listid = null;
     $(document).ready(function(){
        $("#show-delete").hide();
        $("#print-carte").hide();
        

        });
    
    
    
    $(".delete-carte").click(function(){
        var result = confirm("Voulez vous supprimer la carte de ce lecteur ?");
        if (result) {
            //Logic to delete the item
            var idcarte = $(this).attr('data-idcarte');
            $.get('includes/logipam/yii/web/index.php?r=site/deletecarte',{id:idcarte},function(data){
               var valeur = $("#idSearch").val();
                $.get('includes/logipam/yii/web/index.php?r=site/get-lis-lekte-search',{value : valeur},function(data){
                $("#liste-brut").hide();
                $('#liste-recherche').html(data);
                //setTimeout(tablo, 10000);
                }); 
            });
            $("#show-delete").show();
            
            
            
        }
    });
    
    $("#check-all").click(function(){
       if ($('#check-all').is(":checked"))
            {
              $('.choix-lekte').prop('checked',true);
              listid = getValueUsingClass();
              $("#print-carte").show();
            }else{
                $('.choix-lekte').prop('checked',false);
                $("#print-carte").hide();
            }
        
        
    });
    
    $('.choix-lekte').click(function(){
        $("#print-carte").show();
        listid = getValueUsingClass();
        if(!$(".choix-lekte").is(":checked") && !$('#check-all').is(":checked")){
            $("#print-carte").hide();
        }
    });
    
    function getValueUsingClass(){
	/* declare an checkbox array */
	var chkArray = [];
	
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".choix-lekte:checked").each(function() {
		chkArray.push($(this).val());
	});
	
	/* we join the array separated by the comma */
	var selected;
	selected = chkArray.join(',') ;
	
        return selected;
}

    $('#print-carte').click(function(){
        //alert(listid);
        window.open("includes/logipam/yii/web/index.php?r=site/cartepdf&listid="+listid);
        
    });
    
   $("#idSearch").keyup(function(){
            $("#show-delete").hide();
            $("#print-carte").hide();
            var valeur = $("#idSearch").val();
            $.get('includes/logipam/yii/web/index.php?r=site/get-lis-lekte-search',{value : valeur},function(data){
            $("#liste-brut").hide();
            $('#liste-recherche').html(data);
            //setTimeout(tablo, 10000);
            });
            
        });
    
</script>