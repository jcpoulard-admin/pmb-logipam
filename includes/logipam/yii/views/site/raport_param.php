<?php
use app\models\LogipamPresence; 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$sql = "SELECT  lp.empr_cb, e.empr_prenom, e.empr_nom, IF(e.empr_sexe=1,'Masculin','Feminin') AS sexe,e.empr_date_adhesion, e.empr_date_expiration, lp.date_presence FROM logipam_presence lp INNER JOIN empr e ON (e.id_empr = lp.id_empr) "
        . "WHERE lp.date_presence BETWEEN '$date1' AND '$date2' ORDER BY lp.date_presence DESC";
$data_presence = LogipamPresence::findBySql($sql)->asArray()->all(); 

//print_r($data_presence);
?>
<div class='row'>
    <div>
        <table id='tablo-defo' class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Pr&eacute;nom</th>
                    <th>Sexe</th>
                    <th>Code lecteur</th>
                    <th>Date pr&eacutesence</th>
                    <th>Adh&eacute;sion</th>
                    <th>Expiration</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for($i=0;$i<sizeof($data_presence);$i++){
                        ?>
                <tr>
                    <td><?=$data_presence[$i]['empr_nom'] ?></td>
                    <td><?=$data_presence[$i]['empr_prenom'] ?></td>
                    <td><?=$data_presence[$i]['sexe'] ?></td>
                    <td><?=$data_presence[$i]['empr_cb'] ?></td>
                    <td><?= Yii::$app->formatter->asDate($data_presence[$i]['date_presence']) ?></td>
                    <td><?= Yii::$app->formatter->asDate($data_presence[$i]['empr_date_adhesion']) ?></td>
                    <td><?= Yii::$app->formatter->asDate($data_presence[$i]['empr_date_expiration']) ?></td>
                </tr>
            <?php 
                        
                    }
                ?>
            </tbody>
        </table>
    </div>
    
    <script type='text/javascript'>
        $(document).ready(function(){
            $('#tablo-defo').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    //lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  {extend: 'excel', title: 'liste_presence', text: '<i class="fa fa-external-link btn btn-success">Exporter vers Excel</i>'},
                   // {extend: 'excel', title: 'ExampleFile'},
                   // {extend: 'pdf', title: 'ExampleFile'},

                    
                ]

            });

        });
    </script>