<?php
use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
  
     
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2">
            <ul class="sidebar-menu tree" data-widget="tree">
                <li class="treeview">
                 <!--   
                  <a href="#">
                    <i class="fa fa-calendar"></i>
                    <span>Pr&eacute;sence</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                 -->
                  <ul class="treeview-menu" style="display: block;">
                    <li id="takePrezans"><a href="#"><i class="fa fa-plus"></i> Prendre pr&eacute;sence</a></li>
                   <!-- <li id="rapo"><a href="#"><i class="fa fa-area-chart"></i> Rapport</a></li> -->
                  <!--  <li id="liste"><a href="#"><i class="fa fa-list-alt "></i> Historique Pr&eacute;sence</a></li> -->
                    
                  </ul>
                </li>
                <li class="treeview">
                    <!--
                  <a href="#">
                    <i class="fa fa-group"></i>
                    <span>Carte membre</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                    -->
                  <ul class="treeview-menu" style="display: block;">
                    <li id='addPhoto'><a href="#"><i class="fa fa-camera"></i> Ajouter photo</a></li>
                  <!--  <li id="cartePrint"><a href="#"><i class="fa fa-edit"></i>Cartes imprim&eacute;es</a></li> -->
                    <li id="newCard"><a href="#"><i class="fa fa-edit"></i>Cartes imprim&eacute;es</a></li>
                   <!-- <li id="dashboard"><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li> -->
                  </ul>
                </li>
                
            </ul>
           
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10"> <?=  $content ?></div>
        
    </div>

</html>
<?php $this->endPage() ?>        
    
  
