<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $userid
 * @property string $create_dt
 * @property string $last_updated_dt
 * @property string $username
 * @property string $pwd
 * @property string $user_digest
 * @property string $nom
 * @property string $prenom
 * @property int $rights
 * @property string $user_lang
 * @property int $nb_per_page_search
 * @property int $nb_per_page_select
 * @property int $nb_per_page_gestion
 * @property int $param_popup_ticket
 * @property int $param_sounds
 * @property int $param_rfid_activate
 * @property int $param_licence
 * @property int $deflt_notice_statut
 * @property int $deflt_notice_statut_analysis
 * @property int $deflt_integration_notice_statut
 * @property string $xmlta_indexation_lang
 * @property int $deflt_docs_type
 * @property int $deflt_lenders
 * @property string $deflt_styles
 * @property int $deflt_docs_statut
 * @property int $deflt_docs_codestat
 * @property string $value_deflt_lang
 * @property string $value_deflt_fonction
 * @property string $value_deflt_relation
 * @property string $value_deflt_relation_serial
 * @property string $value_deflt_relation_bulletin
 * @property string $value_deflt_relation_analysis
 * @property int $deflt_docs_location
 * @property int $deflt_collstate_location
 * @property int $deflt_bulletinage_location
 * @property int $deflt_resas_location
 * @property int $deflt_docs_section
 * @property string $value_deflt_module
 * @property string $user_email
 * @property int $user_alert_resamail
 * @property int $user_alert_demandesmail
 * @property int $user_alert_subscribemail
 * @property int $user_alert_serialcircmail
 * @property int $deflt2docs_location
 * @property string $deflt_empr_statut
 * @property int $deflt_empr_categ
 * @property int $deflt_empr_codestat
 * @property int $deflt_thesaurus
 * @property int $deflt_concept_scheme
 * @property int $deflt_import_thesaurus
 * @property string $value_prefix_cote
 * @property string $xmlta_doctype
 * @property string $xmlta_doctype_serial
 * @property string $xmlta_doctype_bulletin
 * @property string $xmlta_doctype_analysis
 * @property string $speci_coordonnees_etab
 * @property string $value_email_bcc
 * @property string $value_deflt_antivol
 * @property string $explr_invisible
 * @property string $explr_visible_mod
 * @property string $explr_visible_unmod
 * @property int $deflt3bibli
 * @property int $deflt3exercice
 * @property int $deflt3rubrique
 * @property int $deflt3type_produit
 * @property int $deflt3dev_statut
 * @property int $deflt3cde_statut
 * @property int $deflt3liv_statut
 * @property int $deflt3fac_statut
 * @property int $deflt3sug_statut
 * @property string $environnement
 * @property int $param_allloc
 * @property int $grp_num
 * @property int $deflt_arch_statut
 * @property int $deflt_arch_emplacement
 * @property int $deflt_arch_type
 * @property int $deflt_upload_repertoire
 * @property int $deflt3lgstatdev
 * @property int $deflt3lgstatcde
 * @property int $deflt3receptsugstat
 * @property int $deflt_short_loan_activate
 * @property int $deflt_cashdesk
 * @property int $user_alert_suggmail
 * @property int $deflt_explnum_statut
 * @property int $deflt_notice_replace_keep_categories
 * @property int $deflt_notice_is_new
 * @property int $deflt_agnostic_warehouse
 * @property int $deflt_cms_article_statut
 * @property int $deflt_cms_article_type
 * @property int $deflt_cms_section_type
 * @property int $deflt_scan_request_status
 * @property string $xmlta_doctype_scan_request_folder_record
 * @property int $deflt_camera_empr
 * @property int $deflt_catalog_expanded_caddies
 * @property int $deflt_notice_replace_links
 * @property int $deflt_printer
 * @property int $deflt_opac_visible_bulletinage
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_dt', 'last_updated_dt'], 'safe'],
            [['rights', 'nb_per_page_search', 'nb_per_page_select', 'nb_per_page_gestion', 'param_popup_ticket', 'param_sounds', 'param_rfid_activate', 'param_licence', 'deflt_notice_statut', 'deflt_notice_statut_analysis', 'deflt_integration_notice_statut', 'deflt_docs_type', 'deflt_lenders', 'deflt_docs_statut', 'deflt_docs_codestat', 'deflt_docs_location', 'deflt_collstate_location', 'deflt_bulletinage_location', 'deflt_resas_location', 'deflt_docs_section', 'user_alert_resamail', 'user_alert_demandesmail', 'user_alert_subscribemail', 'user_alert_serialcircmail', 'deflt2docs_location', 'deflt_empr_statut', 'deflt_empr_categ', 'deflt_empr_codestat', 'deflt_thesaurus', 'deflt_concept_scheme', 'deflt_import_thesaurus', 'deflt3bibli', 'deflt3exercice', 'deflt3rubrique', 'deflt3type_produit', 'deflt3dev_statut', 'deflt3cde_statut', 'deflt3liv_statut', 'deflt3fac_statut', 'deflt3sug_statut', 'param_allloc', 'grp_num', 'deflt_arch_statut', 'deflt_arch_emplacement', 'deflt_arch_type', 'deflt_upload_repertoire', 'deflt3lgstatdev', 'deflt3lgstatcde', 'deflt3receptsugstat', 'deflt_short_loan_activate', 'deflt_cashdesk', 'user_alert_suggmail', 'deflt_explnum_statut', 'deflt_notice_replace_keep_categories', 'deflt_notice_is_new', 'deflt_agnostic_warehouse', 'deflt_cms_article_statut', 'deflt_cms_article_type', 'deflt_cms_section_type', 'deflt_scan_request_status', 'deflt_camera_empr', 'deflt_catalog_expanded_caddies', 'deflt_notice_replace_links', 'deflt_printer', 'deflt_opac_visible_bulletinage'], 'integer'],
            [['value_prefix_cote', 'speci_coordonnees_etab', 'environnement'], 'required'],
            [['value_prefix_cote', 'speci_coordonnees_etab', 'explr_invisible', 'explr_visible_mod', 'explr_visible_unmod', 'environnement'], 'string'],
            [['username'], 'string', 'max' => 100],
            [['pwd', 'value_deflt_antivol'], 'string', 'max' => 50],
            [['user_digest', 'user_email', 'value_email_bcc'], 'string', 'max' => 255],
            [['nom', 'prenom', 'value_deflt_module'], 'string', 'max' => 30],
            [['user_lang'], 'string', 'max' => 5],
            [['xmlta_indexation_lang'], 'string', 'max' => 10],
            [['deflt_styles', 'value_deflt_lang', 'value_deflt_fonction', 'value_deflt_relation', 'value_deflt_relation_serial', 'value_deflt_relation_bulletin', 'value_deflt_relation_analysis'], 'string', 'max' => 20],
            [['xmlta_doctype', 'xmlta_doctype_serial', 'xmlta_doctype_bulletin', 'xmlta_doctype_analysis', 'xmlta_doctype_scan_request_folder_record'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'create_dt' => 'Create Dt',
            'last_updated_dt' => 'Last Updated Dt',
            'username' => 'Username',
            'pwd' => 'Pwd',
            'user_digest' => 'User Digest',
            'nom' => 'Nom',
            'prenom' => 'Prenom',
            'rights' => 'Rights',
            'user_lang' => 'User Lang',
            'nb_per_page_search' => 'Nb Per Page Search',
            'nb_per_page_select' => 'Nb Per Page Select',
            'nb_per_page_gestion' => 'Nb Per Page Gestion',
            'param_popup_ticket' => 'Param Popup Ticket',
            'param_sounds' => 'Param Sounds',
            'param_rfid_activate' => 'Param Rfid Activate',
            'param_licence' => 'Param Licence',
            'deflt_notice_statut' => 'Deflt Notice Statut',
            'deflt_notice_statut_analysis' => 'Deflt Notice Statut Analysis',
            'deflt_integration_notice_statut' => 'Deflt Integration Notice Statut',
            'xmlta_indexation_lang' => 'Xmlta Indexation Lang',
            'deflt_docs_type' => 'Deflt Docs Type',
            'deflt_lenders' => 'Deflt Lenders',
            'deflt_styles' => 'Deflt Styles',
            'deflt_docs_statut' => 'Deflt Docs Statut',
            'deflt_docs_codestat' => 'Deflt Docs Codestat',
            'value_deflt_lang' => 'Value Deflt Lang',
            'value_deflt_fonction' => 'Value Deflt Fonction',
            'value_deflt_relation' => 'Value Deflt Relation',
            'value_deflt_relation_serial' => 'Value Deflt Relation Serial',
            'value_deflt_relation_bulletin' => 'Value Deflt Relation Bulletin',
            'value_deflt_relation_analysis' => 'Value Deflt Relation Analysis',
            'deflt_docs_location' => 'Deflt Docs Location',
            'deflt_collstate_location' => 'Deflt Collstate Location',
            'deflt_bulletinage_location' => 'Deflt Bulletinage Location',
            'deflt_resas_location' => 'Deflt Resas Location',
            'deflt_docs_section' => 'Deflt Docs Section',
            'value_deflt_module' => 'Value Deflt Module',
            'user_email' => 'User Email',
            'user_alert_resamail' => 'User Alert Resamail',
            'user_alert_demandesmail' => 'User Alert Demandesmail',
            'user_alert_subscribemail' => 'User Alert Subscribemail',
            'user_alert_serialcircmail' => 'User Alert Serialcircmail',
            'deflt2docs_location' => 'Deflt2docs Location',
            'deflt_empr_statut' => 'Deflt Empr Statut',
            'deflt_empr_categ' => 'Deflt Empr Categ',
            'deflt_empr_codestat' => 'Deflt Empr Codestat',
            'deflt_thesaurus' => 'Deflt Thesaurus',
            'deflt_concept_scheme' => 'Deflt Concept Scheme',
            'deflt_import_thesaurus' => 'Deflt Import Thesaurus',
            'value_prefix_cote' => 'Value Prefix Cote',
            'xmlta_doctype' => 'Xmlta Doctype',
            'xmlta_doctype_serial' => 'Xmlta Doctype Serial',
            'xmlta_doctype_bulletin' => 'Xmlta Doctype Bulletin',
            'xmlta_doctype_analysis' => 'Xmlta Doctype Analysis',
            'speci_coordonnees_etab' => 'Speci Coordonnees Etab',
            'value_email_bcc' => 'Value Email Bcc',
            'value_deflt_antivol' => 'Value Deflt Antivol',
            'explr_invisible' => 'Explr Invisible',
            'explr_visible_mod' => 'Explr Visible Mod',
            'explr_visible_unmod' => 'Explr Visible Unmod',
            'deflt3bibli' => 'Deflt3bibli',
            'deflt3exercice' => 'Deflt3exercice',
            'deflt3rubrique' => 'Deflt3rubrique',
            'deflt3type_produit' => 'Deflt3type Produit',
            'deflt3dev_statut' => 'Deflt3dev Statut',
            'deflt3cde_statut' => 'Deflt3cde Statut',
            'deflt3liv_statut' => 'Deflt3liv Statut',
            'deflt3fac_statut' => 'Deflt3fac Statut',
            'deflt3sug_statut' => 'Deflt3sug Statut',
            'environnement' => 'Environnement',
            'param_allloc' => 'Param Allloc',
            'grp_num' => 'Grp Num',
            'deflt_arch_statut' => 'Deflt Arch Statut',
            'deflt_arch_emplacement' => 'Deflt Arch Emplacement',
            'deflt_arch_type' => 'Deflt Arch Type',
            'deflt_upload_repertoire' => 'Deflt Upload Repertoire',
            'deflt3lgstatdev' => 'Deflt3lgstatdev',
            'deflt3lgstatcde' => 'Deflt3lgstatcde',
            'deflt3receptsugstat' => 'Deflt3receptsugstat',
            'deflt_short_loan_activate' => 'Deflt Short Loan Activate',
            'deflt_cashdesk' => 'Deflt Cashdesk',
            'user_alert_suggmail' => 'User Alert Suggmail',
            'deflt_explnum_statut' => 'Deflt Explnum Statut',
            'deflt_notice_replace_keep_categories' => 'Deflt Notice Replace Keep Categories',
            'deflt_notice_is_new' => 'Deflt Notice Is New',
            'deflt_agnostic_warehouse' => 'Deflt Agnostic Warehouse',
            'deflt_cms_article_statut' => 'Deflt Cms Article Statut',
            'deflt_cms_article_type' => 'Deflt Cms Article Type',
            'deflt_cms_section_type' => 'Deflt Cms Section Type',
            'deflt_scan_request_status' => 'Deflt Scan Request Status',
            'xmlta_doctype_scan_request_folder_record' => 'Xmlta Doctype Scan Request Folder Record',
            'deflt_camera_empr' => 'Deflt Camera Empr',
            'deflt_catalog_expanded_caddies' => 'Deflt Catalog Expanded Caddies',
            'deflt_notice_replace_links' => 'Deflt Notice Replace Links',
            'deflt_printer' => 'Deflt Printer',
            'deflt_opac_visible_bulletinage' => 'Deflt Opac Visible Bulletinage',
        ];
    }
}
