<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publishers".
 *
 * @property int $ed_id
 * @property string $ed_name
 * @property string $ed_adr1
 * @property string $ed_adr2
 * @property string $ed_cp
 * @property string $ed_ville
 * @property string $ed_pays
 * @property string $ed_web
 * @property string $index_publisher
 * @property string $ed_comment
 * @property int $ed_num_entite
 */
class Publishers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publishers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index_publisher', 'ed_comment'], 'string'],
            [['ed_num_entite'], 'integer'],
            [['ed_name', 'ed_adr1', 'ed_adr2', 'ed_web'], 'string', 'max' => 255],
            [['ed_cp'], 'string', 'max' => 10],
            [['ed_ville', 'ed_pays'], 'string', 'max' => 96],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ed_id' => 'Ed ID',
            'ed_name' => 'Ed Name',
            'ed_adr1' => 'Ed Adr1',
            'ed_adr2' => 'Ed Adr2',
            'ed_cp' => 'Ed Cp',
            'ed_ville' => 'Ed Ville',
            'ed_pays' => 'Ed Pays',
            'ed_web' => 'Ed Web',
            'index_publisher' => 'Index Publisher',
            'ed_comment' => 'Ed Comment',
            'ed_num_entite' => 'Ed Num Entite',
        ];
    }
}
