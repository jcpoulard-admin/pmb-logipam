<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "responsability".
 *
 * @property int $id_responsability
 * @property int $responsability_author
 * @property int $responsability_notice
 * @property string $responsability_fonction
 * @property int $responsability_type
 * @property int $responsability_ordre
 */
class Responsability extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'responsability';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['responsability_author', 'responsability_notice', 'responsability_fonction'], 'required'],
            [['responsability_author', 'responsability_notice', 'responsability_type', 'responsability_ordre'], 'integer'],
            [['responsability_fonction'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_responsability' => 'Id Responsability',
            'responsability_author' => 'Responsability Author',
            'responsability_notice' => 'Responsability Notice',
            'responsability_fonction' => 'Responsability Fonction',
            'responsability_type' => 'Responsability Type',
            'responsability_ordre' => 'Responsability Ordre',
        ];
    }
    
    public function authorByNoticeId($notice_id){
        $all_authors = Responsability::findBySql("SELECT r.responsability_author, a.author_name, a.author_rejete  FROM responsability r INNER JOIN authors a ON (a.author_id = r.responsability_author) WHERE r.responsability_notice = $notice_id LIMIT 1")->asArray()->all();
        if(!empty($all_authors)){
            return $all_authors[0]['author_rejete'].' '.$all_authors[0]['author_name'];
        }else{
            return "Pas d'auteur";
        }
    }
}
