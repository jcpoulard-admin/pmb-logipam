<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logipam_visiteur".
 *
 * @property int $id
 * @property int $gender
 * @property string $date_presence
 * @property string $time
 * @property int $location
 */
class LogipamVisiteur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logipam_visiteur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender', 'date_presence', 'time', 'location'], 'required'],
            [['gender', 'location'], 'integer'],
            [['date_presence', 'time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gender' => 'Gender',
            'date_presence' => 'Date Presence',
            'time' => 'Time',
            'location' => 'Location',
        ];
    }
}
