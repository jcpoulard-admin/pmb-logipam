<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pret".
 *
 * @property int $pret_idempr
 * @property int $pret_idexpl
 * @property string $pret_date
 * @property string $pret_retour
 * @property int $pret_arc_id
 * @property int $niveau_relance
 * @property string $date_relance
 * @property int $printed
 * @property string $retour_initial
 * @property int $cpt_prolongation
 * @property string $pret_temp
 * @property int $short_loan_flag
 */
class Pret extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pret';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pret_idempr', 'pret_idexpl', 'pret_arc_id', 'niveau_relance', 'printed', 'cpt_prolongation', 'short_loan_flag'], 'integer'],
            [['pret_idexpl'], 'required'],
            [['pret_date', 'pret_retour', 'date_relance', 'retour_initial'], 'safe'],
            [['pret_temp'], 'string', 'max' => 50],
            [['pret_idexpl'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pret_idempr' => 'Pret Idempr',
            'pret_idexpl' => 'Pret Idexpl',
            'pret_date' => 'Pret Date',
            'pret_retour' => 'Pret Retour',
            'pret_arc_id' => 'Pret Arc ID',
            'niveau_relance' => 'Niveau Relance',
            'date_relance' => 'Date Relance',
            'printed' => 'Printed',
            'retour_initial' => 'Retour Initial',
            'cpt_prolongation' => 'Cpt Prolongation',
            'pret_temp' => 'Pret Temp',
            'short_loan_flag' => 'Short Loan Flag',
        ];
    }
}
