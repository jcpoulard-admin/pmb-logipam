<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logipam_presence".
 *
 * @property integer $id
 * @property integer $id_empr
 * @property string $empr_cb
 * @property string $date_presence
 * @property string $time_presence
 * @property integer $day_presence
 */
class LogipamPresence extends \yii\db\ActiveRecord
{
    
    public $total_jour; 
    public $total_mois;
    public $mois;
    public $label_cat; 
    public $total_cat;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logipam_presence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_empr', 'empr_cb', 'date_presence', 'time_presence', 'day_presence'], 'required'],
            [['id_empr', 'day_presence'], 'integer'],
            [['date_presence', 'time_presence'], 'safe'],
            [['empr_cb'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_empr' => 'Id Empr',
            'empr_cb' => 'Empr Cb',
            'date_presence' => 'Date Presence',
            'time_presence' => 'Time Presence',
            'day_presence' => 'Day Presence',
        ];
    }
    
    public function getCountDay($day_){
        $sql = "SELECT DISTINCT date_presence FROM `logipam_presence` where day_presence = $day_"; 
        $count = LogipamPresence::findBySql($sql)->count(); 
        return $count;
    }
    
    public function getCountMonth($month_){
        $sql = "SELECT  DISTINCT MONTH(date_presence) FROM logipam_presence WHERE MONTH(date_presence) = $month_"; 
        $count = LogipamPresence::findBySql($sql)->count(); 
        return $count;
    }
    
    public function getPresenceByMonthGender($month_,$gender){
        $sql = "SELECT  lp.id, lp.date_presence FROM logipam_presence lp INNER JOIN empr e ON (e.id_empr = lp.id_empr)  WHERE MONTH(lp.date_presence) = $month_ AND e.empr_sexe = $gender";
        $count = LogipamPresence::findBySql($sql)->count(); 
        return $count; 
    }
    
}
