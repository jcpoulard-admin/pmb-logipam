<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notices".
 *
 * @property int $notice_id
 * @property string $typdoc
 * @property string $tit1
 * @property string $tit2
 * @property string $tit3
 * @property string $tit4
 * @property int $tparent_id
 * @property string $tnvol
 * @property int $ed1_id
 * @property int $ed2_id
 * @property int $coll_id
 * @property int $subcoll_id
 * @property string $year
 * @property string $nocoll
 * @property string $mention_edition
 * @property string $code
 * @property string $npages
 * @property string $ill
 * @property string $size
 * @property string $accomp
 * @property string $n_gen
 * @property string $n_contenu
 * @property string $n_resume
 * @property string $lien
 * @property string $eformat
 * @property string $index_l
 * @property int $indexint
 * @property string $index_serie
 * @property string $index_matieres
 * @property string $niveau_biblio
 * @property string $niveau_hierar
 * @property int $origine_catalogage
 * @property string $prix
 * @property string $index_n_gen
 * @property string $index_n_contenu
 * @property string $index_n_resume
 * @property string $index_sew
 * @property string $index_wew
 * @property int $statut
 * @property string $commentaire_gestion
 * @property string $create_date
 * @property string $update_date
 * @property string $signature
 * @property string $thumbnail_url
 * @property string $date_parution
 * @property int $opac_visible_bulletinage
 * @property string $indexation_lang
 * @property int $opac_serialcirc_demande
 * @property int $map_echelle_num
 * @property int $map_projection_num
 * @property int $map_ref_num
 * @property string $map_equinoxe
 * @property int $notice_is_new
 * @property string $notice_date_is_new
 * @property int $num_notice_usage
 *
 * @property NoticesOtherData[] $noticesOtherDatas
 * @property OeuvreMotcles[] $oeuvreMotcles
 * @property OeuvrePhysique[] $oeuvrePhysiques
 * @property OeuvreProvenance[] $oeuvreProvenances
 */
class Notices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tit1', 'tit2', 'tit3', 'tit4', 'n_gen', 'n_contenu', 'n_resume', 'lien', 'index_l', 'index_serie', 'index_matieres', 'index_n_gen', 'index_n_contenu', 'index_n_resume', 'index_sew', 'index_wew', 'commentaire_gestion', 'thumbnail_url'], 'string'],
            [['tparent_id', 'ed1_id', 'ed2_id', 'coll_id', 'subcoll_id', 'indexint', 'origine_catalogage', 'statut', 'opac_visible_bulletinage', 'opac_serialcirc_demande', 'map_echelle_num', 'map_projection_num', 'map_ref_num', 'notice_is_new', 'num_notice_usage'], 'integer'],
           // [['n_gen', 'n_contenu', 'n_resume', 'lien', 'index_l', 'index_matieres', 'commentaire_gestion', 'thumbnail_url'], 'required'],
            [['create_date', 'update_date', 'date_parution', 'notice_date_is_new'], 'safe'],
            [['typdoc'], 'string', 'max' => 2],
            [['tnvol'], 'string', 'max' => 100],
            [['year', 'code'], 'string', 'max' => 50],
            [['nocoll', 'mention_edition', 'npages', 'ill', 'size', 'accomp', 'eformat', 'prix', 'signature', 'map_equinoxe'], 'string', 'max' => 255],
            [['niveau_biblio', 'niveau_hierar'], 'string', 'max' => 1],
            [['indexation_lang'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notice_id' => 'Notice ID',
            'typdoc' => 'Typdoc',
            'tit1' => 'Tit1',
            'tit2' => 'Tit2',
            'tit3' => 'Tit3',
            'tit4' => 'Tit4',
            'tparent_id' => 'Tparent ID',
            'tnvol' => 'Tnvol',
            'ed1_id' => 'Ed1 ID',
            'ed2_id' => 'Ed2 ID',
            'coll_id' => 'Coll ID',
            'subcoll_id' => 'Subcoll ID',
            'year' => 'Year',
            'nocoll' => 'Nocoll',
            'mention_edition' => 'Mention Edition',
            'code' => 'Code',
            'npages' => 'Npages',
            'ill' => 'Ill',
            'size' => 'Size',
            'accomp' => 'Accomp',
            'n_gen' => 'N Gen',
            'n_contenu' => 'N Contenu',
            'n_resume' => 'N Resume',
            'lien' => 'Lien',
            'eformat' => 'Eformat',
            'index_l' => 'Index L',
            'indexint' => 'Indexint',
            'index_serie' => 'Index Serie',
            'index_matieres' => 'Index Matieres',
            'niveau_biblio' => 'Niveau Biblio',
            'niveau_hierar' => 'Niveau Hierar',
            'origine_catalogage' => 'Origine Catalogage',
            'prix' => 'Prix',
            'index_n_gen' => 'Index N Gen',
            'index_n_contenu' => 'Index N Contenu',
            'index_n_resume' => 'Index N Resume',
            'index_sew' => 'Index Sew',
            'index_wew' => 'Index Wew',
            'statut' => 'Statut',
            'commentaire_gestion' => 'Commentaire Gestion',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'signature' => 'Signature',
            'thumbnail_url' => 'Thumbnail Url',
            'date_parution' => 'Date Parution',
            'opac_visible_bulletinage' => 'Opac Visible Bulletinage',
            'indexation_lang' => 'Indexation Lang',
            'opac_serialcirc_demande' => 'Opac Serialcirc Demande',
            'map_echelle_num' => 'Map Echelle Num',
            'map_projection_num' => 'Map Projection Num',
            'map_ref_num' => 'Map Ref Num',
            'map_equinoxe' => 'Map Equinoxe',
            'notice_is_new' => 'Notice Is New',
            'notice_date_is_new' => 'Notice Date Is New',
            'num_notice_usage' => 'Num Notice Usage',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticesOtherDatas()
    {
        return $this->hasMany(NoticesOtherData::className(), ['notices_id' => 'notice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOeuvreMotcles()
    {
        return $this->hasMany(OeuvreMotcles::className(), ['notice_id' => 'notice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOeuvrePhysiques()
    {
        return $this->hasMany(OeuvrePhysique::className(), ['notice_id' => 'notice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOeuvreProvenances()
    {
        return $this->hasMany(OeuvreProvenance::className(), ['notice_id' => 'notice_id']);
    }
}
