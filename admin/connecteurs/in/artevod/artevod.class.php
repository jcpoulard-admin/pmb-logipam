<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: artevod.class.php,v 1.11 2017-07-12 15:15:02 tsamson Exp $

if (stristr($_SERVER['REQUEST_URI'], ".class.php")) die("no access");

global $class_path,$base_path, $include_path;
require_once($class_path."/connecteurs.class.php");
require_once("$class_path/curl.class.php");
if (version_compare(PHP_VERSION,'5','>=') && extension_loaded('xsl')) {
	if (substr(phpversion(), 0, 1) == "5") @ini_set("zend.ze1_compatibility_mode", "0");
	require_once($include_path.'/xslt-php4-to-php5.inc.php');
}

class artevod extends connector {
	//Variables internes pour la progression de la r�cup�ration des notices
	public $profile;			//Profil ArteVOD
	public $n_recu;				//Nombre de notices re�ues
	
	protected $default_enrichment_template; // Template par d�faut de l'enrichissement
	
	public function __construct($connector_path="") {
		parent::__construct($connector_path);
		$xml=file_get_contents($connector_path."/profil.xml");
		$this->profile=_parser_text_no_function_($xml,"ARTEVODCONFIG");
		$this->set_default_enrichment_template();
	}
    
    public function get_id() {
    	return "artevod";
    }
    
    //Est-ce un entrepot ?
	public function is_repository() {
		return 1;
	}
    
   	public function source_get_property_form($source_id) {
    	global $charset, $dbh;
    	
    	$params=$this->get_source_params($source_id);
		if ($params["PARAMETERS"]) {
			//Affichage du formulaire avec $params["PARAMETERS"]
			$vars=unserialize($params["PARAMETERS"]);
			foreach ($vars as $key=>$val) {
				global ${$key};
				${$key}=$val;
			}	
		}
		$searchindexes=$this->profile["SEARCHINDEXES"][0]["SEARCHINDEX"];
		if (!$url) $url=$searchindexes[0]["URL"];
		$form = "";
		if (count($searchindexes) > 1) {
			$form .= "
			<div class='row'>
				<div class='colonne3'>
					<label for='search_indexes'>".$this->msg["artevod_search_in"]."</label>
				</div>
				<div class='colonne_suite'>
					<select name='url' id='url' >";
				for ($i=0; $i<count($searchindexes); $i++) {
					$form.="<option value='".$searchindexes[$i]["URL"]."' ".($url==$searchindexes[$i]["URL"]?"selected":"").">".$this->get_libelle($searchindexes[$i]["COMMENT"])."</option>\n";
				}
				$form.="
				</select>
				</div>
			</div>";
		} else {
			$form .= "
			<input type='hidden' id='url' name='url' value='".$searchindexes[0]["URL"]."' />
			";
		}
		
		// Champ perso de notice � utiliser
		$form .= "<div class='row'>
				<div class='colonne3'><label for='source_name'>".$this->msg["artevod_source_field"]."</label></div>
				<div class='colonne-suite'>
					<select name='cp_field'>";
    	$query = "select idchamp, titre from notices_custom where datatype='integer'";
    	$result = pmb_mysql_query($query, $dbh);
    	if($result && pmb_mysql_num_rows($result)){
    		while($row = pmb_mysql_fetch_object($result)){
    			$form.="
    					<option value='".$row->idchamp."' ".($row->idchamp == $cp_field ? "selected='selected'" : "").">".htmlentities($row->titre,ENT_QUOTES,$charset)."</option>";
    		}
    	}else{
    		$form.="
    					<option value='0'>".$this->msg["artevod_no_field"]."</option>";
    	}
    	$form.="
    				</select>
				</div>
			</div>";
		
    	// Template de l'enrichissement
		$form .= "<div class='row'>
				<div class='colonne3'><label for='source_name'>".$this->msg["artevod_enrichment_template"]."</label></div>
				<div class='colonne-suite'>
					<textarea name='enrichment_template'>".($enrichment_template ? stripslashes($enrichment_template) : stripslashes($this->default_enrichment_template))."</textarea>
				</div>
			</div>";
    	
		$form .= "<div class='row'></div>";
		return $form;
    }
    
    public function make_serialized_source_properties($source_id) {
    	global $url, $cp_field, $enrichment_template;
    	global $del_xsl_transform;
    	
    	$t["url"]=$url;
    	$t["cp_field"] = $cp_field;
    	$t['enrichment_template'] = ($enrichment_template ? $enrichment_template : addslashes($this->default_enrichment_template));
    	
    	$this->sources[$source_id]["PARAMETERS"]=serialize($t);
    }

    /**
     * Formulaire des propri�t�s g�n�rales
     */
    public function get_property_form() {
    	global $charset;
    	$this->fetch_global_properties();
    	//Affichage du formulaire en fonction de $this->parameters
    	if ($this->parameters) {
    		$keys = unserialize($this->parameters);
    		$accesskey= $keys['accesskey'];
    		$secretkey=$keys['secretkey'];
    		$privatekey=$keys['privatekey'];
    	} else {
    		$accesskey="";
    		$secretkey="";
    		$privatekey="";
    	}
    	$r="<div class='row'>
				<div class='colonne3'><label for='accesskey'>".$this->msg["artevod_key"]."</label></div>
				<div class='colonne-suite'><input type='text' id='accesskey' name='accesskey' value='".htmlentities($accesskey,ENT_QUOTES,$charset)."'/></div>
			</div>
			<div class='row'>
				<div class='colonne3'><label for='secretkey'>".$this->msg["artevod_secret_key"]."</label></div>
				<div class='colonne-suite'><input type='password' class='saisie-50em' id='secretkey' name='secretkey' value='".htmlentities($secretkey,ENT_QUOTES,$charset)."'/></div>
			</div>
			<div class='row'>
				<div class='colonne3'><label for='privatekey'>".$this->msg["artevod_private_key"]."</label></div>
				<div class='colonne-suite'><input type='text' class='saisie-50em' id='privatekey' name='privatekey' value='".htmlentities($privatekey,ENT_QUOTES,$charset)."'/></div>
			</div>";
    	return $r;
    }
    
    public function make_serialized_properties() {
    	global $accesskey, $secretkey, $privatekey;
    	//Mise en forme des param�tres � partir de variables globales (mettre le r�sultat dans $this->parameters)
    	$keys = array();
    
    	$keys['accesskey']=$accesskey;
    	$keys['secretkey']=$secretkey;
    	$keys['privatekey']=$privatekey;
    	$this->parameters = serialize($keys);
    }
        
    public function maj_entrepot($source_id, $callback_progress="", $recover=false, $recover_env="") {
    	global $charset, $base_path, $dbh;
    	
    	$this->fetch_global_properties();
    	$keys = unserialize($this->parameters);

		$this->callback_progress = $callback_progress;
		$params = $this->unserialize_source_params($source_id);
		$p = $params["PARAMETERS"];
		$this->source_id = $source_id;
		$this->n_recu = 0;
				
		if(strpos($p["url"],"?")) {
			$url = substr($p["url"],0,strpos($p["url"],"?"));
		} else {
			$url = $p["url"];
		}
			
		$aCurl = new Curl();
		$aCurl->timeout = 60;
		@mysql_set_wait_timeout();
		
		//Authentification Basic
		if (substr($url,0,7) == "http://") {
			$auth_basic = "http://".$keys["accesskey"].":".$keys["secretkey"]."@".substr($url,7);
		} elseif (substr($url,0,8) == "https://") {
			$auth_basic = "https://".$keys["accesskey"].":".$keys["secretkey"]."@".substr($url,8);
		} else {
			$auth_basic = $keys["accesskey"].":".$keys["secretkey"]."@".$url;
		}
		
 		$nb_per_pass = 50;
 		$page_nb = 1;
 		
		$url_temp_auth_basic = $auth_basic."?page_size=".$nb_per_pass."&page_nb=".$page_nb;
		$content = $aCurl->get($url_temp_auth_basic);		
 		$json_content = json_decode($content->body);
 		$this->n_total = count($json_content);
 		
 		if(count($json_content) && $content->headers['Status-Code'] == 200){
 			
 			$query = "select name from notices_custom where idchamp = ".$p['cp_field'];
 			$result = pmb_mysql_query($query, $dbh);
 			if ($row = pmb_mysql_fetch_object($result)) {
 				$cp_artevod = array('cp_artevod' => $row->name);
 			} else {
 				$cp_artevod = array();
 			}
 			$sortir = false;
 			while (!$sortir) {
 				foreach ($json_content as $record) {
 					$statut = $this->rec_record($this->artevod_2_uni($record, $cp_artevod), $source_id, '');
 					if(!$statut) {
 						$sortir = true;
 						break;
 					}
 					$nb++;
 				}
 				$page_nb++; 	
 				//if($page_nb > 10)	$sortir = true;	 				
 				if(!$sortir) {
	 				$url_temp_auth_basic = $auth_basic."?page_size=".$nb_per_pass."&page_nb=".$page_nb;
	 				$content = $aCurl->get($url_temp_auth_basic);	
	 				$json_content = json_decode($content->body);	
 					$this->n_total+= count($json_content); 				
 				}
 			} 			
 		} else {
 			$this->error = true;
 			$this->error_message = $this->msg["artevod_error_auth"];
 		}
		
		return $this->n_recu;
    }
    
    public function progress() {
    	$callback_progress = $this->callback_progress;
		if ($this->n_total) {
			$percent = ($this->n_recu / $this->n_total);
			$nlu = $this->n_recu;
			$ntotal = $this->n_total;
		} else {
			$percent = 0;
			$nlu = $this->n_recu;
			$ntotal = "inconnu";
		}
		call_user_func($callback_progress, $percent, $nlu, $ntotal);
    }
       	
	public function artevod_2_uni($nt, $cp) {
		global $charset;

		$unimarc = array();
		$auttotal = array();
		$naut = 0;
		
		// Construction du 001
		$unimarc["001"][0] = md5(serialize($nt));

		// title
		$unimarc["200"][0]["a"][0] = $nt->title;

		// description (html) -> Notes
		if($nt->description) {
			$unimarc["330"][0]["a"][0] = $nt->description;
		}

		/* themes (array)
				(
                    [0] => Com�die romantique
                )
		*/
		$themes = $nt->themes;
		if (count($themes)) {
			foreach($themes as $theme) {
				//$unimarc[""][] = $theme;
			}
		}

		// productionYear (2014)
		if ($nt->productionYear) {			
			//$unimarc[""][0][""][0] = $nt->productionYear;
		}		
		
		// posterUrl (http://prod-mednum.universcine.com/media/58/da/58da559ff0fa3.jpeg)
		if ($nt->posterUrl) {			
			$unimarc["896"][0]["a"][0] = $nt->posterUrl;
		}		
		
		// url (http://prod-mednum.universcine.com/une-saison-a-la-juilliard-school)
		if ($nt->url) {			
			$unimarc["856"][0]["u"][0] = $nt->url;
		}

		// trailerUrl (http://media.universcine.com/0f/a3/0fa3f154-c07a-11e3-bfdd-e59cda21687c.mp4)
		if ($nt->trailerUrl) {
			$unimarc["897"][0]["a"][0] = $nt->trailerUrl;
			$unimarc["897"][0]["b"][0] = $nt->trailerUrl;
		}
		
		// duration (6240)
		if($nt->duration) {
			$unimarc["215"][0]["a"][0] = $nt->duration. ' s';
		}
		
		/* audioLanguages (array)
                (
                    [0] => stdClass Object
                        (
                            [type] => Language
                            [code] => eng
                        )
                )
		*/
		$audioLanguages = $nt->audioLanguages;
		if (count($audioLanguages)) {
			for ($i=0; $i<count($audioLanguages); $i++) {
				$autt = array();
				$autt["a"][0] = $audioLanguages[$i]->code;
				$unimarc['101'][] = $autt;
			}
		}

		/* directors (array)
                (
                    [0] => stdClass Object
                        (
                            [type] => Person
                            [fullName] => Max Nichols
                            [familyName] => Nichols
                            [givenName] => Max
                        )
                )
		*/		    
		$authors = $nt->directors;
		if (count($authors)) {
			if (($naut + count($authors)) > 1) {
				$autf = "701"; 
			}else {
				$autf = "700";
			}
			for ($i=0; $i<count($authors); $i++) {
				$autt = array();
				$autt["a"][0] = $authors[$i]->familyName;
				$autt["b"][0] = $authors[$i]->givenName;
				$autt["4"][0] = "651";
				$unimarc[$autf][] = $autt;
				$auttotal[] = $authors[$i];
			}
			$naut+= count($authors);			
		}

		/* actors (array)
                (
                    [0] => stdClass Object
                        (
                            [type] => Person
                            [fullName] => Analeigh Tipton
                            [familyName] => Tipton
                            [givenName] => Analeigh
                        )
                )
		*/
		$authors = $nt->actors;
		if (count($authors)) {
			$autf = "702";
			for ($i=0; $i<count($authors); $i++) {
				$autt = array();
				$autt["a"][0] = $authors[$i]->familyName;
				$autt["b"][0] = $authors[$i]->givenName;
				$autt["4"][0] = "005";
				$unimarc[$autf][] = $autt;
				$auttotal[] = $authors[$i];
			}
			$naut+= count($authors);
		}
		
		// publicationDate (2017-03-28)
		if ($nt->publicationDate) {		
			if(!($publicationDate = formatdate($nt->publicationDate))) {
				$publicationDate = $nt->publicationDate;
			}	
			$unimarc["210"][0]["d"][0] = $publicationDate;
		}		
		
		/* genres (array)
						(
							[0] => Documentaire
							[1] => Th��tre, cirque et danse	                    
						)
		*/
		$genres = $nt->genres;
		if (count($genres)) {
			foreach($genres as $genre) {
				$unimarc["610"][0]["a"][] = $genre;
			}
		}
		
		// productionCountry (US)
		if ($nt->productionCountry) {			
			$unimarc["210"][0]["a"][0] = $nt->productionCountry;
		}	
			
		/* codes  => Array
                (
                    [0] => stdClass Object
                        (
                            [type] => Le meilleur du cin�ma
                            [code] => 622040
                        )
                )
        */
		$codes = $nt->codes; 
		if (count($codes)) {
			for ($i=0; $i<count($codes); $i++) {
				$autt = array();
				$autt["t"][0] = $codes[$i]->type;
				$autt["v"][0] = $codes[$i]->code;
				$unimarc['410'][] = $autt; // Collection
			}
		}

		/* medias (array)[0] => stdClass Object
                        (
                            [type] => POSTER
                            [url] => http://prod-mednum.universcine.com/media/58/da/58da57b51bd44.jpeg
                            [modificationDate] => 2017-03-28T14:32:22
                        )
		*/
		$medias = $nt->medias;
		if (count($medias)) {
			for ($i=0; $i<count($medias); $i++) {
				$autt = array();
				$autt["a"][0] = $medias[$i]->url;
				$autt["b"][0] = $medias[$i]->type.'_'.basename($medias[$i]->url);
				$unimarc['897'][] = $autt;
			}
		}
		
		// rate (4)
		if ($nt->rate) {			
			//$unimarc[""][0][""][0] = $nt->rate;
		}		
		
		/* comments (array)(
                    [0] => excellent on pourrait croire un woody allen
                    [1] => 
                )
		*/
		$comments = $nt->comments;
		if (count($comments)) {
			$unimarc["300"][0]["a"][0] = '';
			foreach($comments as $comment) {		
				$unimarc["300"][0]["a"][0].= $comment.' ';
			}
		}
		
		// commentsLibrary (array)
		$comments = $nt->commentsLibrary;
		if (count($comments)) {
			$unimarc["327"][0]["a"][0] = '';
			foreach($comments as $comment) {				
				$unimarc["327"][0]["a"][0].= $comment.' ';
			}
		}
		
		// bonus (array)
		$comments = $nt->bonus;
		if (count($comments)) {
			foreach($comments as $comment) {
				//$unimarc[""][0][""][] = $comment;
			}
		}
		
		if($cp['cp_artevod']) {
			$unimarc["900"][0]["a"][0] = $nt->id;
			$unimarc["900"][0]["n"][0] = $cp['cp_artevod'];
		}
		
		$unimarc["801"][0]["a"][0] = 'FR';
		$unimarc["801"][0]["b"][0] = 'ArteVOD';

		return $unimarc;
	} 
        
    public function rec_record($record, $source_id, $search_id) {
    	global $charset, $base_path, $dbh, $url, $search_index;

    	$date_import = date("Y-m-d H:i:s",time());
    	
    	//Recherche du 001
    	$ref = $record["001"][0];
    	//Mise � jour
    	if ($ref) {
    		$ref_exists = $this->has_ref($source_id, $ref);
    		if ($ref_exists) return false;
    		
    		
    		
    		//Si conservation des anciennes notices, on regarde si elle existe
    		$ref_exists = false;
    		if (!$this->del_old) {
    			$ref_exists = $this->has_ref($source_id, $ref);
    		}
    		//Si pas de conservation des anciennes notices, on supprime
    		if ($this->del_old) {
    			$this->delete_from_entrepot($source_id, $ref);
    			$this->delete_from_external_count($source_id, $ref);
    		}
    		if (($this->del_old) || ((!$this->del_old)&&(!$ref_exists))) {
    			//Insertion de l'ent�te
				$n_header["rs"] = "*";
				$n_header["ru"] = "*";
				$n_header["el"] = "1";
				$n_header["bl"] = "m";
				$n_header["hl"] = "0";
				$n_header["dt"] = "g";

				//R�cup�ration d'un ID
				$recid = $this->insert_into_external_count($source_id, $ref);
				foreach($n_header as $hc=>$code) {
					$this->insert_header_into_entrepot($source_id, $ref, $date_import, $hc, $code, $recid, $search_id);
				}

				$field_order=0;
				foreach ($record as $field=>$val) {
					for ($i=0; $i<count($val); $i++) {
						if (is_array($val[$i])) {
							foreach ($val[$i] as $sfield=>$vals) {
								for ($j=0; $j<count($vals); $j++) {
									if ($charset!="utf-8")  $vals[$j] = utf8_decode($vals[$j]);
									$this->insert_content_into_entrepot($source_id, $ref, $date_import, $field, $sfield, $field_order, $j, $vals[$j], $recid, $search_id);
								}
							}
						} else {
							if ($charset!="utf-8")  $vals[$i] = utf8_decode($vals[$i]);
							$this->insert_content_into_entrepot($source_id, $ref, $date_import, $field, '', $field_order, 0, $val[$i], $recid, $search_id);
						}
						$field_order++;
					}
				}
				$this->rec_isbd_record($source_id, $ref, $recid);    		
    		}
    		$this->n_recu++;
    		$this->progress();
    	}
    	return true;
    }

    public function enrichment_is_allow(){
    	return true;
    }
	
	public function getTypeOfEnrichment($source_id){
		$type['type'] = array(
			array(
				"code" => "artevod",
				"label" => $this->msg['artevod_vod']
			)
		);		
		$type['source_id'] = $source_id;
		return $type;
	}
	
	public function getEnrichment($notice_id,$source_id,$type="",$enrich_params=array()){
		$enrichment= array();
		return $enrichment;
	}
	
	public function getEnrichmentHeader(){
		$header= array();
		return $header;
	}
	
	/**
	 * D�finit le template par d�faut de l'enrichissement
	 */
	private function set_default_enrichment_template() {
		$this->default_enrichment_template = "{* Template par d�faut *}
<div class='enrichment_artevod_container' style='width:400px;'>
	
	<div class='enrichment_artevod_mediatheque_numerique' style='text-align:center;'>
		<img src='./images/mediatheque_numerique.png' style='margin:5px;' title='M�diath�que num�rique' />
	</div>
				
	{* titre *}
	{% if film.title %}
		<h3 class='enrichment_artevod_title'>{{ film.title }}
		{% if film.original_title %}
			({{ film.original_title }})
		{% endif %}
		</h3>
	{% else %}
		{% if film.original_title %}
			<h3 class='enrichment_artevod_title'>{{ film.original_title }}</h3>
		{% endif %}
	{% endif %}
	
	{* lien vers la ressource *}
	{% if film.externaluri %}
		<p class='enrichment_artevod_externaluri'>
			<a href='{{ film.externaluri }}' target='_BLANK'>Voir le programme</a>
		</p>
	{% endif %}
	
	{* genres *}
	{% for genre in film.genres %}
		{% if loop.first %}<p class='enrichment_artevod_genres'>{% endif %}
		{{ genre }}{%if !loop.last %}, {% endif %}
	
		{* sous-genres *}
		{% for subgenre in film.subgenres %}
			{% if loop.first %}/{% endif %}
			{{ subgenre }}{%if !loop.last %}, {% else %}</p>{% endif %}
		{% endfor %}
	{% endfor %}
	
	{* auteurs *}
	{% for author in film.authors %}
		{% if loop.first %}<p class='enrichment_artevod_authors'>De {% endif %}
		{{ author }}{%if !loop.last %}, {% else %}</p>{% endif %}
	{% endfor %}
	
	{* acteurs *}
	{% for actor in film.actors %}
		{% if loop.first %}<p class='enrichment_artevod_actors'>Avec {% endif %}
		{{ actor }}{%if !loop.last %}, {% else %}</p>{% endif %}
	{% endfor %}
	
	{* infos *}
	<p class='enrichment_artevod_infos'>
	{% if film.production_year %}<strong>Ann�e :</strong> {{ film.production_year }}.{% endif %}
	{% if film.production_countries %} <strong>Pays :</strong> {{ film.production_countries }}.{% endif %}
	{% if film.languages %} <strong>Langue :</strong> {{ film.languages }}.{% endif %}
	{% if film.target_audience %} <strong>Public :</strong> {{ film.target_audience }}.{% endif %}
	</p>
	
	{* description *}
	{% if film.description %}
		<p class='enrichment_artevod_description' style='font-weight:bold;font-size:1.2em;'>{{ film.description }}</p>
	{% endif %}
	
	{* r�sum� *}
	{% if film.body %}
		<p class='enrichment_artevod_body'>{{ film.body }}</p>
	{% endif %}
	
	{* dur�e *}
	{% if film.duration.format_value %}
		<p class='enrichment_artevod_duration'><strong>Dur�e :</strong> {{ film.duration.format_value }}</p>
	{% endif %}
	
	{* extrait *}
	{% for trailer in film.trailers %}
		<video class='enrichment_artevod_trailer' width='400px' controls src='{{ trailer }}' style='margin-top:5px;margin-bottom:5px;'>{{ 'Voir l\'extrait' |links_to film.externaluri }}</video>
	{% endfor %}
	
	{* photos *}
	{% for photo in film.photos %}
		{% if loop.first %}<div class='enrichment_artevod_photos'>{% endif %}
			<div style='height:110px;width:49%;float:left;clear:none;margin-right:1%;margin-bottom:2px;text-align:center;'>
				<img style='max-height:110px;max-width:100%;' src='{{ photo }}' title='photo' class='enrichment_artevod_photo' />
			</div>
		{% if loop.last %}</div>{% endif %}
	{% endfor %}
				
	<div class='row'></div>
</div>";
	}
}// class end