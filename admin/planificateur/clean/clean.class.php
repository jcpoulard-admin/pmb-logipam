<?php
// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: clean.class.php,v 1.13 2017-07-10 15:50:02 dgoron Exp $

global $class_path;
require_once($class_path."/scheduler/scheduler_task.class.php");
require_once($class_path."/netbase/netbase.class.php");
		
class clean extends scheduler_task {
	
	public function execution() {
		global $dbh, $msg, $charset, $PMBusername;
		global $acquisition_active,$pmb_indexation_docnum;
		
		if (SESSrights & ADMINISTRATION_AUTH) {
			$parameters = $this->unserialize_task_params();
			$percent = 0;
			//progression
			$p_value = (int) 100/count($parameters["clean"]);
			$this->add_section_report($this->msg["planificateur_clean"]);
			$result="";
			foreach ($parameters["clean"] as $clean) {
				$this->listen_commande(array(&$this,"traite_commande"));
				if($this->statut == WAITING) {
					$this->send_command(RUNNING);
				}
				if ($this->statut == RUNNING) {
					switch ($clean) {
						case INDEX_GLOBAL:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_index_global"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_indexGlobal')) {
								$result .= $this->proxy->pmbesClean_indexGlobal();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"indexGlobal","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_NOTICES:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_index_notices"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_indexNotices')) {
								$result .= $this->proxy->pmbesClean_indexNotices();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"indexNotices","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_AUTHORS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_authors"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanAuthors')) {
								$result .= $this->proxy->pmbesClean_cleanAuthors();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanAuthors","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_PUBLISHERS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_editeurs"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanPublishers')) {
								$result .= $this->proxy->pmbesClean_cleanPublishers();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanPublishers","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_COLLECTIONS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_collections"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanCollections')) {
								$result .= $this->proxy->pmbesClean_cleanCollections();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanCollections","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_SUBCOLLECTIONS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_subcollections"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanSubcollections')) {
								$result .= $this->proxy->pmbesClean_cleanSubcollections();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanSubcollections","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_CATEGORIES:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_categories"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanCategories')) {
								$result .= $this->proxy->pmbesClean_cleanCategories();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanCategories","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_SERIES:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_series"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanSeries')) {
								$result .= $this->proxy->pmbesClean_cleanSeries();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanSeries","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_TITRES_UNIFORMES:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_titres_uniformes"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanTitresUniformes')) {
								$result .= $this->proxy->pmbesClean_cleanTitresUniformes();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanTitresUniformes","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_INDEXINT:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_indexint"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanIndexint')) {
								$result .= $this->proxy->pmbesClean_cleanIndexint();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanIndexint","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_RELATIONS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_relations"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanRelations')) {
								$result .= $this->proxy->pmbesClean_cleanRelations();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanRelations","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_NOTICES:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_expl"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanNotices')) {
								$result .= $this->proxy->pmbesClean_cleanNotices();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanNotices","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_ACQUISITIONS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_reindex_acq"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if ($acquisition_active) {
								if (method_exists($this->proxy, 'pmbesClean_indexAcquisitions')) {
									$result .= $this->proxy->pmbesClean_indexAcquisitions();
									$percent += $p_value;
									$this->update_progression($percent);	
								} else {
									$result .= "<p>".sprintf($msg["planificateur_function_rights"],"indexAcquisitions","pmbesClean",$PMBusername)."</p>";
								}
							} else {
								$result .= "<p>".$this->msg["clean_acquisition"]."</p>";
							}
							$result .= "</td></tr>";
							break;
						case GEN_SIGNATURE_NOTICE:
							$result .= "<tr><th>".htmlentities($msg["gen_signature_notice"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_genSignatureNotice')) {
								$result .= $this->proxy->pmbesClean_genSignatureNotice();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"genSignatureNotice","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case GEN_PHONETIQUE:
							$result .= "<tr><th>".htmlentities($msg["gen_phonetique"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_genPhonetique')) {
								$result .= $this->proxy->pmbesClean_genPhonetique();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"genPhonetique","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case NETTOYAGE_CLEAN_TAGS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_clean_tags"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_nettoyageCleanTags')) {
								$result .= $this->proxy->pmbesClean_nettoyageCleanTags();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"nettoyageCleanTags","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_CATEGORIES_PATH:
							$result .= "<tr><th>".htmlentities($msg["clean_categories_path"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanCategoriesPath')) {
								$result .= $this->proxy->pmbesClean_cleanCategoriesPath();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanCategoriesPath","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case GEN_DATE_PUBLICATION_ARTICLE:
							$result .= "<tr><th>".htmlentities($msg["gen_date_publication_article"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_genDatePublicationArticle')) {
								$result .= $this->proxy->pmbesClean_genDatePublicationArticle();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"genDatePublicationArticle","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case GEN_DATE_TRI:
							$result .= "<tr><th>".htmlentities($msg["gen_date_tri"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_genDateTri')) {
								$result .= $this->proxy->pmbesClean_genDateTri();
								$percent += $p_value;
								$this->update_progression($percent);	
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"genDateTri","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_DOCNUM:
							$result .= "<tr><th>".htmlentities($msg["docnum_reindexer"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if ($pmb_indexation_docnum) {
								if (method_exists($this->proxy, 'pmbesClean_indexDocnum')) {
									$result .= $this->proxy->pmbesClean_indexDocnum();
									$percent += $p_value;
									$this->update_progression($percent);	
								} else {
									$result .= "<p>".sprintf($msg["planificateur_function_rights"],"indexDocnum","pmbesClean",$PMBusername)."</p>";
								}
							} else {
								$result .= "<p>".$this->msg["clean_indexation_docnum"]."</p>";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_OPAC_SEARCH_CACHE:
							$result .= "<tr><th>".htmlentities($msg["cleaning_opac_search_cache"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							$query = "truncate table search_cache";
							if(pmb_mysql_query($query,$dbh)){
								$query = "optimize table search_cache";
								if(pmb_mysql_query($query,$dbh)){
									$result.= "OK";
								}else{
									$result.= "OK";
								}
								$percent += $p_value;
								$this->update_progression($percent);
							}else{
								$result.= "KO";
							}
							$result .= "</td></tr>";
							break;
						case CLEAN_CACHE_AMENDE:
							$result .= "<tr><th>".htmlentities($msg["cleaning_cache_amende"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							$query = "truncate table cache_amendes";
							if(pmb_mysql_query($query,$dbh)){
								$query = "optimize table cache_amendes";
								if(pmb_mysql_query($query,$dbh)){
									$result.= "OK";
								}else{
									$result.= "OK";
								}
								$percent += $p_value;
								$this->update_progression($percent);
							}else{
								$result.= "KO";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_RDFSTORE:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_rdfstore_reindexation"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanRdfStore')) {
								$result .= $this->proxy->pmbesClean_cleanRdfStore();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanRdfStore","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_SYNCHRORDFSTORE:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_synchrordfstore_reindexation"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanSynchroRdfStore')) {
								$result .= $this->proxy->pmbesClean_cleanSynchroRdfStore();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanSynchroRdfStore","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_FAQ:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_reindex_faq"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanFAQ')) {
								$result .= $this->proxy->pmbesClean_cleanFAQ();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanFAQ","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_CMS:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_reindex_cms"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanCMS')) {
								$result .= $this->proxy->pmbesClean_cleanCMS();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanCMS","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_CONCEPT:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_reindex_concept"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_cleanConcept')) {
								$result .= $this->proxy->pmbesClean_cleanConcept();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"cleanConcept","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case HASH_EMPR_PASSWORD:
							$result .= "<tr><th>".htmlentities($msg["hash_empr_password"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_hashEmprPassword')) {
								$result .= $this->proxy->pmbesClean_hashEmprPassword();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"hashEmprPassword","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
						case INDEX_AUTHORITIES:
							$result .= "<tr><th>".htmlentities($msg["nettoyage_index_authorities"], ENT_QUOTES, $charset)."</th></tr>";
							$result .= "<tr><td>";
							if (method_exists($this->proxy, 'pmbesClean_indexAuthorities')) {
								$result .= $this->proxy->pmbesClean_indexAuthorities();
								$percent += $p_value;
								$this->update_progression($percent);
							} else {
								$result .= "<p>".sprintf($msg["planificateur_function_rights"],"indexAuthorities","pmbesClean",$PMBusername)."</p>";
							}
							$result .= "</td></tr>";
							break;
					}
				}
			}
			$this->report[] = $result;
		} else {
			$this->add_rights_bad_user_report();
		}
		
	}
}


