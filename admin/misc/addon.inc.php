<?php
// +-------------------------------------------------+
//  2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: addon.inc.php,v 1.5.8.16 2017-11-03 15:46:37 ngantier Exp $

if (stristr($_SERVER['REQUEST_URI'], ".inc.php")) die("no access");

function traite_rqt($requete="", $message="") {

	global $dbh,$charset;
	$retour="";
	if($charset == "utf-8"){
		$requete=utf8_encode($requete);
	}
	$res = pmb_mysql_query($requete, $dbh) ; 
	$erreur_no = pmb_mysql_errno();
	if (!$erreur_no) {
		$retour = "Successful";
	} else {
		switch ($erreur_no) {
			case "1060":
				$retour = "Field already exists, no problem.";
				break;
			case "1061":
				$retour = "Key already exists, no problem.";
				break;
			case "1091":
				$retour = "Object already deleted, no problem.";
				break;
			default:
				$retour = "<font color=\"#FF0000\">Error may be fatal : <i>".pmb_mysql_error()."<i></font>";
				break;
			}
	}		
	return "<tr><td><font size='1'>".($charset == "utf-8" ? utf8_encode($message) : $message)."</font></td><td><font size='1'>".$retour."</font></td></tr>";
}
echo "<table>";

/******************** AJOUTER ICI LES MODIFICATIONS *******************************/

switch ($pmb_bdd_subversion) {
	case '0' :
		//JP - Impression tickets de pr�t via raspberry pi
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'pmb' and sstype_param='printer_name' "))){
			$rqt = "update parametres set comment_param=CONCAT(comment_param,'\n\nSi l\'imprimante est connect�e � un Raspberry Pi, indiquer l\'ip et le port\nExemple : raspberry@192.168.0.82:3000') where type_param='pmb' and sstype_param='printer_name' " ;
			echo traite_rqt($rqt,"update parameters pmb_printer_name");
		}
	case '1' :
		// JP - Rectification index sur index_author / author_type de la table authors
		$rqt = "alter table authors drop index i_index_author_author_type";
		echo traite_rqt($rqt,"alter table authors drop index i_index_author_author_type");
		$rqt = "alter table authors add index i_index_author_author_type (index_author (350), author_type)";
		echo traite_rqt($rqt,"alter table authors add index i_index_author_author_type");
	case '2' :
		//JP - Ajout d'une colonne commentaire dans la table des recherches pr�d�finies
		if (!pmb_mysql_num_rows(pmb_mysql_query("SHOW COLUMNS FROM search_perso LIKE 'search_comment'"))){
			$rqt = "alter table search_perso add search_comment text not null";
			echo traite_rqt($rqt,"alter table search_perso add search_comment");
		}
	case '3' :
		//JP - Export des informations de documents num�riques dans les notices en unimarc pmb xml
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'opac' and sstype_param='export_allow_expl' "))){
			$rqt = "update parametres set comment_param='Exporter les exemplaires et les documents num�riques avec les notices :\n 0 : Aucun\n 1 : Uniquement les exemplaires\n 2 : Uniquement les documents num�riques\n 3 : Les exemplaires et les documents num�riques' where type_param='opac' and sstype_param='export_allow_expl' " ;
			echo traite_rqt($rqt,"update parameters opac_export_allow_expl");
		}
	case '4' :
		//JP - Param�tre g�rant l'ent�te de la fiche lecteur
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'empr' and sstype_param='header_format' "))){
			$rqt = "update parametres set comment_param='Champs qui seront affich�s dans l\'ent�te de la fiche emprunteur. S�parer les valeurs par des virgules. \nPour les champs personnalis�s, saisir les identifiants. Les autres valeurs possibles sont les propri�t�s de la classe PHP \"pmb/opac_css/classes/emprunteur.class.php\".' where type_param='empr' and sstype_param='header_format' " ;
			echo traite_rqt($rqt,"update parameters empr_header_format");
		}
	case '5' :
		//JP & MB - Ajout d'index sur la table cms_cache_cadres
		$req="SHOW INDEX FROM cms_cache_cadres WHERE key_name LIKE 'i_cache_cadre_create_date'";
		$res=pmb_mysql_query($req);
		if($res && (pmb_mysql_num_rows($res) == 0)){
			$rqt = "alter table cms_cache_cadres add index i_cache_cadre_create_date(cache_cadre_create_date)";
			echo traite_rqt($rqt,"alter table cache_cadre_create_date add index i_cache_cadre_create_date");
		}
	case '6':
		//VT - Param�tre de d�finition du style dojo en gestion
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'pmb' and sstype_param='dojo_gestion_style' "))==0){
			$rqt = "INSERT INTO parametres ( type_param, sstype_param, valeur_param, comment_param,section_param,gestion)
			VALUES ( 'pmb', 'dojo_gestion_style', 'claro', 'Styles disponibles: tundra, claro, flat, nihilo, soria','', 0)";
			echo traite_rqt($rqt,"insert pmb_dojo_gestion_style into parametres");
		}
	case '7':
		// VT & AP - Ajout d'un droit sur le statut de lecteur pour les contributions
		$rqt = "alter table empr_statut add allow_contribution int unsigned not null default 0";
		echo traite_rqt($rqt,"alter table empr_statut add allow_contribution");
			
		// AP & VT - Modification du nom du parametre empr_contribution en empr_contribution_area
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'gestion_acces' and sstype_param='empr_contribution' "))){
			$rqt = "update parametres set sstype_param='empr_contribution_area' where type_param='gestion_acces' and sstype_param='empr_contribution' " ;
			echo traite_rqt($rqt,"update parameters set sstype_param='empr_contribution_area' where sstype_param='empr_contribution'");
		}
			
		// AP & VT - Modification du nom du parametre empr_contribution_def en empr_contribution_area_def
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'gestion_acces' and sstype_param='empr_contribution_def' "))){
			$rqt = "update parametres set sstype_param='empr_contribution_area_def' where type_param='gestion_acces' and sstype_param='empr_contribution_def' " ;
			echo traite_rqt($rqt,"update parameters set sstype_param='empr_contribution_area_def' where sstype_param='empr_contribution_def'");
		}
			
		// AP & VT - Ajout du parametre empr_contribution_scenario dans les droits d'acces
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'gestion_acces' and sstype_param='empr_contribution_scenario' "))==0){
			$rqt = "INSERT INTO parametres ( type_param, sstype_param, valeur_param, comment_param,section_param,gestion)
				VALUES ( 'gestion_acces', 'empr_contribution_scenario', '0', 'Gestion des droits d\'acc�s des emprunteurs aux sc�narios de contribution\n0 : Non.\n1 : Oui.', '', 0)";
			echo traite_rqt($rqt,"insert empr_contribution_scenario into parametres");
		}
			
		// AP & VT - Ajout du parametre empr_contribution_scenario_def dans les droits d'acces
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'gestion_acces' and sstype_param='empr_contribution_scenario_def' "))==0){
			$rqt = "INSERT INTO parametres ( type_param, sstype_param, valeur_param, comment_param,section_param,gestion)
				VALUES ( 'gestion_acces', 'empr_contribution_scenario_def', '0', 'Valeur par d�faut en modification de scenario de contribution pour les droits d\'acc�s emprunteurs - sc�narios\n0 : Recalculer.\n1 : Choisir.', '', 0)";
			echo traite_rqt($rqt,"insert empr_contribution_scenario into parametres");
		}
			
		// AP & VT - Ajout du parametre contribution_moderator_empr dans les droits d'acces
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'gestion_acces' and sstype_param='contribution_moderator_empr' "))==0){
			$rqt = "INSERT INTO parametres ( type_param, sstype_param, valeur_param, comment_param,section_param,gestion)
				VALUES ( 'gestion_acces', 'contribution_moderator_empr', '0', 'Gestion des droits d\'acc�s des mod�rateurs sur les contributeurs\n0 : Non.\n1 : Oui.', '', 0)";
			echo traite_rqt($rqt,"insert contribution_moderator_empr into parametres");
		}
			
		// AP & VT - Ajout du parametre contribution_moderator_empr_def dans les droits d'acces
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'gestion_acces' and sstype_param='contribution_moderator_empr_def' "))==0){
			$rqt = "INSERT INTO parametres ( type_param, sstype_param, valeur_param, comment_param,section_param,gestion)
				VALUES ( 'gestion_acces', 'contribution_moderator_empr_def', '0', 'Valeur par d�faut en modification d\'emprunteur pour les droits d\'acc�s mod�rateur - emprunteur\n0 : Recalculer.\n1 : Choisir.', '', 0)";
			echo traite_rqt($rqt,"insert contribution_moderator_empr_def into parametres");
		}
			
		// AP & VT - Suppression du statut sur les formulaires de contribution
		if (pmb_mysql_num_rows(pmb_mysql_query("SHOW COLUMNS FROM contribution_area_forms LIKE 'form_status'"))){
			$rqt = "ALTER TABLE contribution_area_forms drop column form_status";
			echo traite_rqt($rqt,"ALTER TABLE contribution_area_forms drop column form_status");
		}
			
		// AP & VT - Ajout d'un statut sur les espaces de contributions
		if (pmb_mysql_num_rows(pmb_mysql_query("SHOW COLUMNS FROM contribution_area_areas LIKE 'area_status'"))==0){
			$rqt = "ALTER TABLE contribution_area_areas add column area_status int(10) unsigned not null default 1";
			echo traite_rqt($rqt,"ALTER TABLE contribution_area_areas add column area_status");
		}
	case '8':
		//JP - Index incorrect sur faq_questions_words_global_index
		$rqt ="alter table faq_questions_words_global_index drop primary key";
		echo traite_rqt($rqt,"alter table faq_questions_words_global_index drop primary key");
		$rqt ="alter table faq_questions_words_global_index add primary key (id_faq_question,code_champ,code_ss_champ,num_word,position,field_position)";
		echo traite_rqt($rqt,"alter table faq_questions_words_global_index add primary key");
		if ($faq_active) {
			// Info de r�indexation
			$rqt = " select 1 " ;
			echo traite_rqt($rqt,"<b><a href='".$base_path."/admin.php?categ=netbase' target=_blank>VOUS DEVEZ REINDEXER LA FAQ / YOU MUST REINDEX THE FAQ : Admin > Outils > Nettoyage de base > R�indexer la faq</a></b> ") ;
		}
	case '9':
		// TS - VT - Afficher template m�me sans donn�es
		$rqt = "ALTER TABLE frbr_cadres ADD cadre_display_empty_template tinyint(1) UNSIGNED NOT NULL default 1" ;
		echo traite_rqt($rqt,"ALTER TABLE frbr_cadres ADD cadre_display_empty_template");
	case '10':		
		// NG - Si concept actif, attribution des droits de modification des concepts CONCEPTS_AUTH, 
		// � tous les utilisateurs ayant acces � THESAURUS_AUTH,
		// seulement si aucun utilisateur n'a ce droit sur les concepts
		if($thesaurus_concepts_active) {
			if (!pmb_mysql_num_rows(pmb_mysql_query("select 1 from users where rights>=4194304"))) {				
				$rqt = "update users set rights=rights+4194304 where rights<4194304 and rights&2048";
				echo traite_rqt($rqt, "update users add rights CONCEPTS_AUTH");
			}
		}
	case '11':
		// NG - Ajout template pour les impressions de panier en OPAC
		$rqt="CREATE TABLE IF NOT EXISTS print_cart_tpl (
	            id_print_cart_tpl int unsigned not null auto_increment primary key,
	            print_cart_tpl_name varchar(255) not null default '',
	            print_cart_tpl_header text not null,
	            print_cart_tpl_footer text not null
       	        )";
		echo traite_rqt($rqt,"create table print_cart_tpl");
			
		// Ajout du param�tre indiquant le template � utiliser pour les impressions de panier en OPAC
		if (pmb_mysql_num_rows(pmb_mysql_query("select 1 from parametres where type_param= 'opac' and sstype_param='print_cart_header_footer' "))==0){
			$rqt = "INSERT INTO parametres ( type_param, sstype_param, valeur_param, comment_param,section_param,gestion)
				VALUES ( 'opac', 'print_cart_header_footer', '', 'Identifiant du template � utiliser pour ins�rer un en-t�te et un pied de page en impression de panier. Les templates sont cr��s en Administration > Template de Mail > Template impression de panier.','h_cart', 0)";
			echo traite_rqt($rqt,"insert opac_print_cart_header_footer into parametres");
		}
}











/******************** JUSQU'ICI **************************************************/
/* PENSER � faire +1 au param�tre $pmb_subversion_database_as_it_shouldbe dans includes/config.inc.php */
/* COMMITER les deux fichiers addon.inc.php ET config.inc.php en m�me temps */

echo traite_rqt("update parametres set valeur_param='".$pmb_subversion_database_as_it_shouldbe."' where type_param='pmb' and sstype_param='bdd_subversion'","Update to $pmb_subversion_database_as_it_shouldbe database subversion.");
echo "<table>";