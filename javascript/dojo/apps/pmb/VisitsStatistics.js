// +-------------------------------------------------+
// � 2002-2004 PMB Services / www.sigb.net pmb@sigb.net et contributeurs (voir www.sigb.net)
// +-------------------------------------------------+
// $Id: VisitsStatistics.js,v 1.2 2017-01-17 15:01:18 apetithomme Exp $

define([
        "dojo/_base/declare",
        "dojo/_base/lang",
        "dojo/request",
        "dojo/query",
        "dojo/on",
        "dojo/dom-attr",
        "dojo/dom",
        "dojo/ready"
], function(declare, lang, request, query, on, domAttr, dom, ready){
	return declare(null, {
		constructor: function() {
			query('.visits_statistics_button').forEach(lang.hitch(this, this.addEventOnButton));
			query('.visits_statistics_input').forEach(lang.hitch(this, this.addEventOnInput));
			ready(this, this.overrideSaveCircParam);
		},
		
		addEventOnButton: function(node) {
			on(node, 'click', lang.hitch(this, this.updateCounter));
		},
		
		addEventOnInput: function(node) {
			on(node, 'change', lang.hitch(this, this.counterChanged));
		},
		
		updateCounter: function(e) {
			var action = domAttr.get(e.target, 'action');
			var counterType = domAttr.get(e.target, 'counter_type');
			var input = dom.byId('visits_statistics_' + counterType + '_input');
			if ((action == 'remove') && (parseInt(input.value) > 0)) {
				input.value = parseInt(input.value) - 1;
			}
			if (action == 'add') {
				input.value = parseInt(input.value) + 1;
			}
			this.updateDatabase(counterType, input.value);
		},
		
		counterChanged: function(e) {
			var counterType = domAttr.get(e.target, 'counter_type');
			e.target.value = parseInt(e.target.value);
			this.updateDatabase(counterType, e.target.value);
		},
		
		updateDatabase: function(counterType, value) {
			request.post('ajax.php?module=ajax&categ=visits_statistics&sub=update', {
				data: {
					counter_type: counterType,
					value: value
				}
			});
		},
		
		overrideSaveCircParam: function() {
			if (typeof(save_circ_params) == 'function') {
				var old_save_circ_params = save_circ_params;
				save_circ_params = function() {
					old_save_circ_params();
					request.get('ajax.php?module=ajax&categ=visits_statistics&sub=get_data', {
						handleAs: 'json'
					}).then(function(data) {
						query('.visits_statistics_input').forEach(function(node) {
							var counter_type = domAttr.get(node, 'counter_type');
							if (data[counter_type]) {
								node.value = data[counter_type];
							} else {
								node.value = 0;
							}
						});
					});
				}
			}
		}
	});
});